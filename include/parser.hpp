#ifndef PARSER_HPP
#define PARSER_HPP

#include <string>

/** Minimalistic parser for fastq files */
void parse(std::string filename, std::string& res, int k);

#endif
