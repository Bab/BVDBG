#ifndef DOMINATOR_HPP
#define DOMINATOR_HPP

#include "path_compression.hpp"
#include "bvdbg_wrapper.hpp"
#include "dfs.hpp"
#include <assert.h>
#include <vector>

/** A class to compute the dominator structure of a flow graph. If right is
 *  false, act on the reverse of the input graph */
template<class t_graph = bvdbg_wrapper<>,
         bool right = 1>
class dominator {
  public:
    typedef t_graph                     graph_type;
    typedef dfs<graph_type, right>      dfs_type;
    typedef typename t_graph::size_type size_type;
  private:
    const graph_type *g;
    const dfs_type *df;
    std::vector<size_type> pre, post;
    
    /** A class used for path compression */
    class B {
      public:
      static std::vector<size_type> *semi;
      size_type v;
      B(size_type a = 0): v(a) {}
      friend B operator* (B a, B b) {
        if ((*semi)[a.v] < (*semi)[b.v]) {
          return a;
        } else {
          return b;
        }
      }
    };
    
  public:
    dominator() {};
    dominator(const graph_type &g0, const dfs_type &df0): g(&g0), df(&df0) {
      // Initialize semi
      size_type n = df->n_nodes();
      size_type max_idx = g->max_index();
      std::vector<size_type> semi(n);
      for (size_type i = 0; i < n; ++i) {
        semi[i] = i;
      }
      std::vector<size_type> dom(n, -1);
      
      B::semi = &semi;
      
      std::vector<std::stack<size_type>> bucket(n);
      
      // Initialize default_label for path_compression
      std::vector<B> default_label(max_idx);
      for (size_type i = 0; i < n; ++i) {
        default_label[i] = B(i);
      }
      path_compression<B> p_compress(default_label);
      for (size_type i = n; i > 0; --i) {
        size_type idx = df->i_pre(i-1);
        size_type parent = df->pre(df->parent(idx));
        // get in edges
        std::vector<size_type> edges(g->max_order(), 0);
        size_type degree = g->template next_nodes<!right>(edges, idx);
        for (int j = 0; j < degree; ++j) {
          size_type u = p_compress.eval(df->pre(edges[j])).v;
          if (semi[u] < semi[i-1]) {
            semi[i-1] = semi[u];
          }
        }
        bucket[semi[i-1]].push(i-1);
        p_compress.link(parent, i-1);
        while (!bucket[parent].empty()) {
          size_type v = bucket[parent].top();
          bucket[parent].pop();
          size_type u = p_compress.eval(v).v;
          if (semi[u] < semi[v]) {
            dom[v] = u;
          } else {
            dom[v] = parent;
          }
        }
      }
      for (size_type i = 0; i < n; ++i) {
        //size_type j = df->i_pre(i);
        if (dom[i] > n) {
        }
        if (dom[i] != semi[i]) {
          dom[i] = dom[dom[i]];
        }
      }
      dom[0] = -1;
      
      std::vector<std::vector<size_type> > i_dom(n);
      
      
      // Invert the pointers of dom
      for (size_type i = 1; i < n; ++i) {
        i_dom[dom[i]].push_back(i);
      }
      
      
      // make a depth_first search of the dominator tree
      // updating pre and post in the process
      pre = std::vector<size_type>(n, -1);
      post = std::vector<size_type>(n, -1);
      std::stack<size_type> stk;
      stk.push(0);
      size_type count_pre = 0;
      size_type count_post = 0;
      while(!stk.empty()) {
        size_type x = stk.top();
        if (pre[x] == -1) {
          pre[x] = count_pre;
          ++count_pre;
          for (size_type i = 0; i < i_dom[x].size(); ++i) {
            stk.push(i_dom[x][i]);
          }
        } else {
          post[x] = count_post;
          ++count_post;
          stk.pop();
        }
      }
    }
    
    /** Returns true if x dominates y in the flow graph */
    bool dominate(size_type x, size_type y) const {
      size_type px = df->pre(x);
      size_type py = df->pre(y);
      return pre[px] < pre[py] && post[py] < post[px];
    }
      
};

template<class t_graph,
         bool right>
std::vector<typename t_graph::size_type> *dominator<t_graph, right>::B::semi = NULL;
#endif
