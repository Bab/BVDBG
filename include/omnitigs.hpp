#ifndef OMNITIGS_HPP
#define OMNITIGS_HPP

#include "trie.hpp"
#include "bvdbg.hpp"
#include "bvdbg_wrapper.hpp"
#include "strong_bridges.hpp"
#include <assert.h>
#include <vector>
#include <stack>
#include <map>


/** Computes the omnitigs in the wrapped graph */
template <class t_graph = bvdbg_wrapper<> >
class wrapped_omnitigs {
  public:
    typedef t_graph                         graph_type;
    typedef typename graph_type::size_type  size_type;
    typedef std::pair<size_type, size_type> edge_type;
    typedef std::vector<size_type>          path_type;
    
  private:
    const graph_type *g;
    std::vector<std::vector<size_type> > content;
    
  private:
    
    /** Return the smallest index i of path such that
     *  path[i], ... path[path.size()-1] is an omnitig */
    size_type longest_suffix(const std::set<edge_type> str_bridges,
                             const path_type &path) const {
      size_type l = path.size();
      size_type max_idx = g->max_index();
      assert(l > 1);
      
      size_type r = path[l-2];
      size_type e = path[l-1];
      
      assert(str_bridges.count({r, e}) > 0);
      
      // We will start a depth first search of g starting from r without taking
      // the edge (r, e)
      std::vector<bool> visited(max_idx, false);
      std::vector<size_type> next(g->max_order());
      std::stack<size_type> stk;
      stk.push(r);
      // This make sure that we don't take the edge (r, e).
      // As (r, e) should be a strong bridge, we should only be able to get to
      // e from r.
      visited[e] = true;
      
      while (!stk.empty()) {
        size_type v = stk.top();
        stk.pop();
        if (!visited[v]) {
          visited[v] = true;
          int ord = g->template next_nodes<1>(next, v);
          for (int i = 0; i < ord; ++i) {
            stk.push(next[i]);
          }
        }
      }
      
      // Now we can compute the result
      for (size_type j = l-2; j > 0; --j) {
        int ord = g->template next_nodes<0>(next, path[j]);
        for (int i = 0; i < ord; ++i) {
          if (next[i] != path[j-1] && visited[next[i]]) {
            return j;
          }
        }
      }
      return 0;
    }
    
    /** return a node v such that the path from u to v is univocal.
     *  if right is false, then operate on the reverse graph */
    template<bool right>
    path_type find_univocal(size_type u) {
      size_type v = u;
      path_type res;
      res.push_back(u);
      std::vector<size_type> next(g->max_order());
      int ord = g->template next_nodes<right>(next, v);
      while (ord == 1) {
        v = next[0];
        res.push_back(v);
        if (v == u) {
          // Only usefull when g is limmited to a single cycle
          break;
        }
        ord = g->template next_nodes<right>(next, v);
      }
      return res;
    }
    
    /** return a path from u to v */
    template<bool right>
    path_type find_path(size_type u, size_type v) {
      std::vector<bool> visited(g->max_index(), false);
      std::vector<size_type> next(g->max_order());
      std::stack<size_type> stk;
      path_type path;
      stk.push(u);
      path.push_back(u);
      while (!stk.empty()) {
        size_type w = stk.top();
        if (w == v) {
          path.push_back(w);
          return path;
        }
        if (!visited[w]) {
          visited[w] = true;
          path.push_back(w);
          int ord = g->template next_nodes<right>(next, w);
          for (int i = 0; i < ord; ++i) {
            stk.push(next[i]);
          }
        } else {
          if (w == path[path.size() - 1]) {
            path.pop_back();
          }
          stk.pop();
        }
      }
      assert(false);
      return {};
    }
    
    size_type last_branch(const path_type &path) {
      assert(path.size() > 1);
      size_type i = path.size() - 2;
      std::vector<size_type> next (g->max_order());
      int ord = g->template next_nodes<1>(next, path[i]);
      while (i > 0 && ord == 1) {
        --i;
        ord = g->template next_nodes<1>(next, path[i]);
      }
      return i;
    }
    
    path_type &omnitig_ending_with(edge_type e,
                  std::set<edge_type> &str_bridges,
                  std::map<edge_type, path_type> &memoized_res) {
      if (memoized_res.count(e)) {
        return memoized_res[e];
      }
      size_type s = std::get<0>(e);
      size_type t = std::get<1>(e);
      
      std::vector<size_type> siblings(g->max_order());
      int ord = g->template next_nodes<1>(siblings, s);
      assert(ord > 1);
      size_type t2 = siblings[0] == t ? siblings[1] : siblings[0];
      
      path_type p2 = find_univocal<0>(s);
      if (ord == 2) {
        path_type p_uni = find_univocal<1>(t2);
        if ((p_uni.size() > 1 && p_uni[p_uni.size() - 1] == s) || t2 == s) {
          path_type res;
          for (int i = 0; i < p2.size(); ++i) {
            res.push_back(p2[p2.size() - i - 1]);
          }
          for (int i = 0; i < p_uni.size(); ++i) {
            res.push_back(p_uni[i]);
          }
          res.push_back(t);
          memoized_res[e] = res;
          return memoized_res[e];
        }
      }
      // Not in the original
      if (s == t) {
        int ord = g->template next_nodes<0>(siblings, s);
        if (ord == 2) {
          size_type s2 = siblings[0] == s ? siblings[1] : siblings[0];
          path_type p3 = find_univocal<0>(s2);
          path_type res;
          for (int i = 0; i < p3.size(); ++i) {
            res.push_back(p3[p3.size() - i - 1]);
          }
          res.push_back(s);
          res.push_back(s);
          memoized_res[e] = res;
          return memoized_res[e];
        }
      }
      if (str_bridges.count({s, t}) == 0) {
        path_type res;
        for (int i = 0; i < p2.size(); ++i) {
          res.push_back(p2[p2.size() - i - 1]);
        }
        res.push_back(t);
        memoized_res[e] = res;
        return memoized_res[e];
      }
      path_type p = find_path<1>(t, s);
      size_type f_idx = last_branch(p);
      p.push_back(t);
      size_type w2_idx = longest_suffix(str_bridges, p);
      if (w2_idx > f_idx) {
        path_type res;
        for (int i = w2_idx; i < p.size(); ++i) {
          res.push_back(p[i]);
        }
        memoized_res[e] = res;
        return memoized_res[e];
      }
      path_type w3 = omnitig_ending_with({p[f_idx], p[f_idx+1]}, str_bridges, memoized_res);
      for (int i = f_idx + 2; i < p.size(); ++i) {
        w3.push_back(p[i]);
      }
      size_type w3_idx = longest_suffix(str_bridges, w3);
      path_type res;
      for (int i = w3_idx; i < w3.size(); ++i) {
        res.push_back(w3[i]);
      }
      memoized_res[e] = res;
      return memoized_res[e];
    }
    
    void complete(path_type &path) {
      path_type right_part = find_univocal<1>(path[path.size()-1]);
      for (int i = 1; i < right_part.size(); ++i) {
        path.push_back(right_part[i]);
      }
    }
  
  public:
    wrapped_omnitigs() {}
    wrapped_omnitigs(graph_type &g0): g(&g0) {
      std::set<edge_type> str_bridges = strong_bridges(*g);
      std::map<edge_type, path_type> memoized_res;
      
      size_type v = g->get_r();
      std::vector<bool> visited(g->max_index(), false);
      std::vector<size_type> next(g->max_order());
      int ord;
      std::stack<size_type> stk;
      stk.push(v);
      while(!stk.empty()) {
        v = stk.top();
        stk.pop();
        if (!visited[v]) {
          visited[v] = true;
          ord = g->template next_nodes<1>(next, v);
          if (ord == 1) {
            stk.push(next[0]);
          } else {
            for (int i = 0; i < ord; ++i) {
              stk.push(next[i]);
              omnitig_ending_with({v, next[i]}, str_bridges, memoized_res);
            }
          }
        }
      }
      if (memoized_res.size() == 0) {
        v = g->get_r();
        path_type res = {v};
        complete(res);
        content.push_back(res);
      }
      else {
        for (auto it = memoized_res.begin(); it != memoized_res.end(); ++it) {
          complete(it->second);
          content.push_back(it->second);
        }
      }
    }
    
    std::vector<path_type> get_content() {
      return content;
    }
};

/** Returns the omnitigs of the subgraph of order k of the input bvdbg */
template<class t_bvdbg = bvdbg<>>
std::vector<std::string>
get_omnitigs(const t_bvdbg &bg, typename t_bvdbg::lcs_value_type k) {
  typedef typename t_bvdbg::size_type size_type;
  using graph_type = bvdbg_wrapper<t_bvdbg>;
  using wrapped_omn_type = wrapped_omnitigs<graph_type>;
  
  graph_type g(bg, k);
  wrapped_omn_type w_omn(g);
  std::vector<std::vector<size_type> > int_res = w_omn.get_content();
  // Convert into string
  trie t;
  for (int i = 0; i < int_res.size(); ++i) {
    std::string s;
    assert(int_res[i].size() > 1);
    s = bg.get_pattern(g.orig_node(int_res[i][0]));
    for (int j = 1; j < int_res[i].size(); ++j) {
      s.push_back(bg.template get_last_char<1>(g.orig_node(int_res[i][j])));
    }
    t.add(s);
  }
  return t.recover();
}
  
  

#endif
