#ifndef PATH_COMPRESSION_HPP
#define PATH_COMPRESSION_HPP

#include <vector>
#include <stack>
#include <assert.h>

// TODO: this is a simple non-optimized version

/** A class for path compression, class B should provide an operator *
 *  associative (not necessarily commutative)
 */
template<class B>
class path_compression {
  private:
    std::vector<unsigned int> ancestor;
    std::vector<B> label;
  
  public:
    path_compression() {}
    path_compression(std::vector<B> default_label) {
      unsigned int n = default_label.size();
      ancestor = std::vector<unsigned int>(n, n);
      label = default_label;
    }
    
    void compress(unsigned int i) {
      unsigned int n = ancestor.size();
      unsigned int j = i;
      std::stack<unsigned int> st;
      while (ancestor[j] < n) {
        st.push(j);
        j = ancestor[j];
      }
      unsigned int r = j;
      B b = label[r];
      while (!(st.empty())) {
        j = st.top();
        st.pop();
        b = b * label[j];
        ancestor[j] = r;
        label[j] = b;
      }
    }
    
    void link(unsigned int i, unsigned int j) {
      assert(ancestor[j] == ancestor.size());
      if (i != j) {
        ancestor[j] = i;
      }
    }
    
    B eval(unsigned int i) {
      compress(i);
      return label[i];
    }
};

#endif
