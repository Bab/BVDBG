#ifndef MY_WT_HPP
#define MY_WT_HPP
#include "my_wt_int.hpp"
#include "my_wt_pc.hpp"
#include <sdsl/wt_blcd.hpp>
#include <sdsl/wt_hutu.hpp>

template<class t_bitvector1   = sdsl::bit_vector,
         class t_rank1        = typename t_bitvector1::rank_1_type,
         class t_select_one1  = typename t_bitvector1::select_1_type,
         class t_select_zero1 = typename t_bitvector1::select_0_type,
         class t_tree_strat1  = sdsl::byte_tree<>
         >
using my_wt_blcd = my_wt_pc<sdsl::balanced_shape,
                            t_bitvector1,
                            t_rank1,
                            t_select_one1,
                            t_select_zero1,
                            t_tree_strat1>;

template<class t_bitvector1   = sdsl::bit_vector,
         class t_rank1        = typename t_bitvector1::rank_1_type,
         class t_select_one1  = typename t_bitvector1::select_1_type,
         class t_select_zero1 = typename t_bitvector1::select_0_type,
         class t_tree_strat1  = sdsl::byte_tree<>
         >
using my_wt_hutu = my_wt_pc<sdsl::hutu_shape,
                            t_bitvector1,
                            t_rank1,
                            t_select_one1,
                            t_select_zero1,
                            t_tree_strat1>;


#endif
