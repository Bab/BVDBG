#ifndef TRIE_HPP
#define TRIE_HPP

#include <map>
#include <string>
#include <vector>

/** A class for storing a set of left-maximal strings */
class trie {
  private:
    std::string prefix;
    std::map<char, trie> sons;
    
  public:
    trie();
    
    /** Add the string s[start ... s.size()[ to the trie, if another string of
     *  the trie is already prefixed by s[start ... s.size()[, has no effect */
    void add(const std::string s, int start = 0);
    
    
    /** Recover only the left-maximal strings of the trie in lexicographic order
     * */
    std::vector<std::string> recover() const;
};

#endif
