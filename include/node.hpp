#ifndef NODE_HPP
#define NODE_HPP
#include <tuple>

/** The class for the nodes of the bvdbg */
template<typename t_size, typename t_lcs_value>
class node {
  public:
    typedef t_size      size_type;
    typedef t_lcs_value lcs_value_type;
    
    /** Order of the node */
    lcs_value_type k;
    
    /** Beginning of the intervals of the pattern in both half-bvdbg */
    std::pair<size_type, size_type> b;
};

/** A lighter node class in the case where we have to manage a large collection
 *  of nodes for which we know k */
template<class t_node>
class small_node {
  public:
    typedef t_node                          node_type;
    typedef typename t_node::size_type      size_type;
    typedef typename t_node::lcs_value_type lcs_value_type;
    
    std::pair<size_type, size_type> b;
  
    small_node() {}
    
    small_node(node_type v): b(v.b) {}
    
    node_type full(lcs_value_type k) const {
      node_type res;
      res.b = b;
      res.k = k;
      return res;
    }
};

#endif
