#ifndef HALF_BVDBG_HPP
#define HALF_BVDBG_HPP

#include <vector>
#include <string>
#include <set>
#include <assert.h>

#include <sdsl/wavelet_trees.hpp>
#include <sdsl/bit_vectors.hpp>
#include <sdsl/suffix_arrays.hpp>
#include <sdsl/lcp.hpp>

#include "my_wt.hpp"

/** A half of a bvdbg, really an augmented variable order de Bruijn graph */
template <class t_wt_edges  = my_wt_hutu<>,
          class t_wt_lcs    = my_wt_hutu<>,
          class t_bitvector = sdsl::bit_vector,
          class t_rank      = typename t_bitvector::rank_1_type,
          class t_select    = typename t_bitvector::select_1_type
          >
class half_bvdbg {
  public:
    typedef t_wt_edges                          wt_edges_type;
    typedef t_wt_lcs                            wt_lcs_type;
    typedef typename wt_edges_type::size_type   size_type;
    typedef typename wt_edges_type::value_type  edge_value_type;
    typedef typename wt_lcs_type::value_type    lcs_value_type;
    typedef t_bitvector                         bitvector_type;
    typedef t_rank                              rank_type;
    typedef t_select                            select_type;
  
  private:
    /** Maximum order of the Bvdbg */
    lcs_value_type k_max;
    
    // Alphabet and its rank and support data structure are owned
    // by the full bvdbg
    
    /** Bitvector describing the subset of characters present in the text */
    bitvector_type  *alphabet;
    rank_type       *alphabet_rank;
    select_type     *alphabet_select;
    
    /** char_count[i] contains the number of character strictly less than the
     *  i-th character present in the text (to be used with alphabet) */
    sdsl::int_vector<64> char_count;
    
    /** Contains the edges labels of the bvdbg */
    wt_edges_type wt_edges;
    
    /** Contains the lcs in a wt */
    wt_lcs_type   wt_lcs;
    
    /** Encodes the in-degree of the nodes */
    bitvector_type bv_in_degree;
    rank_type      bv_in_degree_rank;
    select_type    bv_in_degree_select;
    
    /** Encodes the out-degree of the nodes */
    bitvector_type bv_out_degree;
    rank_type      bv_out_degree_rank;
    select_type    bv_out_degree_select;
    
  public:
    
    half_bvdbg() {}
    
    half_bvdbg(const half_bvdbg& other):
        k_max(other.k_max),
        alphabet(other.alphabet),
        alphabet_rank(other.alphabet_rank),
        alphabet_select(other.alphabet_select),
        char_count(other.char_count),
        wt_edges(other.wt_edges),
        wt_lcs(other.wt_lcs),
        bv_in_degree(other.bv_in_degree),
        bv_in_degree_rank(other.bv_in_degree_rank),
        bv_in_degree_select(other.bv_in_degree_select),
        bv_out_degree(other.bv_out_degree),
        bv_out_degree_rank(other.bv_out_degree_rank),
        bv_out_degree_select(other.bv_out_degree_select) {
      // make the rank and select data structure point to the correct vector
      bv_in_degree_rank.set_vector(&bv_in_degree);
      bv_in_degree_select.set_vector(&bv_in_degree);
      bv_out_degree_rank.set_vector(&bv_out_degree);
      bv_out_degree_select.set_vector(&bv_out_degree);
    }
    
    half_bvdbg& operator=(const half_bvdbg& other) {
      if (this != &other) {
        k_max = other.k_max;
        alphabet = other.alphabet;
        alphabet_rank = other.alphabet_rank;
        alphabet_select = other.alphabet_select;
        char_count = other.char_count;
        wt_edges = other.wt_edges;
        wt_lcs = other.wt_lcs;
        bv_in_degree = other.bv_in_degree;
        bv_in_degree_rank = other.bv_in_degree_rank;
        bv_in_degree_select = other.bv_in_degree_select;
        bv_out_degree = other.bv_out_degree;
        bv_out_degree_rank = other.bv_out_degree_rank;
        bv_out_degree_select = other.bv_out_degree_select;
        // make the rank and select data structure point to the correct vector
        bv_in_degree_rank.set_vector(&bv_in_degree);
        bv_in_degree_select.set_vector(&bv_in_degree);
        bv_out_degree_rank.set_vector(&bv_out_degree);
        bv_out_degree_select.set_vector(&bv_out_degree);
      }
      return *this;
    }
    
    /** Constructor taking the pointer to the data in bvdbg and the reverse of
     * the string for the forward half_bvdbg (the string itself for the
     * backward one)
     */
    half_bvdbg(lcs_value_type k,
               const std::string &reverse_input,
               bitvector_type *alphabet,
               rank_type *alphabet_rank,
               select_type *alphabet_select):
               k_max(k),
               alphabet(alphabet),
               alphabet_rank(alphabet_rank),
               alphabet_select(alphabet_select) {
      
      size_type len = reverse_input.size();
      
      // Construction of suffix_array and lcp_array
      sdsl::csa_bitcompressed<> suffix_array;
      sdsl::construct_im(suffix_array, reverse_input.c_str(), 1);
      sdsl::lcp_bitcompressed<> lcp_array;
      sdsl::construct_im(lcp_array, reverse_input.c_str(), 1);

      sdsl::int_vector<8> edge_vector(len+1, 0);
      sdsl::int_vector<8> lcs_vector(len, 0);
      bv_out_degree = bitvector_type(len+1, 0);
      bv_in_degree = bitvector_type(len+1, 0);
      
      size_type i_edges = 0;
      size_type i_nodes = 0;
      size_type i_in_degree = 0;
      std::set<edge_value_type> labels;
      for (size_type i_sa = 0; i_sa < suffix_array.size() - 1; ++i_sa) {
        edge_value_type c = 0;
        size_type pos = suffix_array[i_sa];
        if (pos > 0) {
          c = (edge_value_type) reverse_input[pos-1];
        }
        assert((*alphabet)[c] == 1);
        labels.insert(c);
        if (lcp_array[i_sa+1] < k) {
          // We will change node at next iteration
          for (typename std::set<edge_value_type>::iterator
               labels_it = labels.begin();
               labels_it != labels.end(); ++labels_it) {
            edge_vector[i_edges] = *labels_it;
            ++i_edges;
          }
          labels.clear();
          
          bv_out_degree[i_edges-1] = 1;
          bv_in_degree[i_in_degree] = 1;
          ++i_in_degree;
          
          lcs_vector[i_nodes] = lcp_array[i_sa+1];
          ++i_nodes;
        }
        else if (lcp_array[i_sa+1] == k) {
          // We will change node's incoming edge at next iteration
          ++i_in_degree;
        }
      }
      // Last iteration of the loop
      edge_value_type c = 0;
      size_type pos = suffix_array[suffix_array.size() - 1];
      if (pos > 0) {
        c = (edge_value_type) reverse_input[pos-1];
      }
      assert((*alphabet)[c] == 1);
      labels.insert(c);
      for (typename std::set<edge_value_type>::iterator
           labels_it = labels.begin();
           labels_it != labels.end(); ++labels_it) {
        edge_vector[i_edges] = *labels_it;
        ++i_edges;
      }
      bv_out_degree[i_edges-1] = 1;
      bv_in_degree[i_in_degree] = 1;
      ++i_in_degree;
      
      // Resize the vectors
      assert(i_in_degree == i_edges);
      
      edge_vector.resize(i_edges);
      lcs_vector.resize(i_nodes);
      bv_out_degree.resize(i_edges);
      bv_in_degree.resize(i_edges);
      
      // Construct the sdsl data structure needed
      sdsl::construct_im(wt_edges, edge_vector, 0);
      sdsl::construct_im(wt_lcs, lcs_vector, 0);
      bv_in_degree_rank = rank_type(&(this->bv_in_degree));
      bv_in_degree_select = select_type(&(this->bv_in_degree));
      bv_out_degree_rank = rank_type(&(this->bv_out_degree));
      bv_out_degree_select = select_type(&(this->bv_out_degree));
      
      // Initialisation of char_count
      int alphabet_size = (*alphabet_rank)(alphabet->size());
      char_count = sdsl::int_vector<64>(alphabet_size, 0);
      // Start by counting each distict character in wt_edges
      for (size_type i = 0; i < wt_edges.size(); ++i) {
        int char_index = (*alphabet_rank)(wt_edges[i]);
        ++char_count[char_index];
      }
      // Then sum all the obtained results
      size_type sum = 0;
      for (size_type i = 0; i < char_count.size(); ++i) {
        size_type temp = char_count[i];
        char_count[i] = sum;
        sum += temp;
      }
    }
    
    ~half_bvdbg() {}
    
    /** Accessors */
    lcs_value_type get_k_max() const {
      return k_max;
    }
    
    const bitvector_type *get_alphabet() const {
      return alphabet;
    }
    
    const sdsl::int_vector<64> &get_char_count() const {
      return char_count;
    }
    
    const wt_edges_type &get_edges() const {
      return wt_edges;
    }
    
    const wt_lcs_type &get_lcs() const {
      return wt_lcs;
    }
    
    const bitvector_type &get_in_degree() const {
      return bv_in_degree;
    }
    
    const bitvector_type &get_out_degree() const {
      return bv_out_degree;
    }
    
    /** Returns the last characters at position b */
    edge_value_type get_c(size_type b) const {
      size_type node_b = bv_out_degree_rank(b);
      size_type in_index  = 0;
      if (node_b > 0) {
        in_index = bv_in_degree_select(node_b) + 1;
      }
      size_type c_b = 0;
      size_type c_e = (*alphabet_rank)(alphabet->size());
      while (c_b + 1 < c_e) {
        size_type c_m = (c_b + c_e) / 2;
        if (char_count[c_m] <= in_index) {
          c_b = c_m;
        } else {
          c_e = c_m;
        }
      }
      return (*alphabet_select)(c_e);
    }
    
    /** returns the end of the interval starting at b for a node of order k */
    size_type get_e(size_type b, lcs_value_type k) const {
      size_type node_b = bv_out_degree_rank(b);
      std::pair<bool, size_type> res_e;
      res_e = wt_lcs.template find_closest<1, 0>(node_b, k);
      if (std::get<0>(res_e)) {
        return bv_out_degree_select(std::get<1>(res_e) + 1) + 1;
      } else {
        return wt_edges.size();
      }
    }
    
    /** Tell if there is an edge labelled c leaving the current node */
    bool check_edge(size_type b, lcs_value_type k, edge_value_type c) const {
      size_type e = get_e(b, k);
      return (wt_edges.rank(b, c) < wt_edges.rank(e, c));
    }
    
    /** Follow an edge labelled c from b, returning a node of order new_k
     *  */
    size_type follow_edge(size_type b,
                          edge_value_type c,
                          lcs_value_type new_k) const {
      assert(new_k <= k_max);
      // How many out-edges labelled c are before the edge we will follow
      size_type edge_rank = wt_edges.rank(b, c);
      
      // How many in_edges are before the one we will follow
      size_type in_index = char_count[(*alphabet_rank)(c)] + edge_rank;
      
      // How many nodes are before our destination
      size_type node_index = bv_in_degree_rank(in_index);
      std::pair<bool, size_type> previous_lower =
          wt_lcs.template find_closest<0, 0>(node_index, new_k);
      if (std::get<0>(previous_lower)) {
        size_type new_node_b = std::get<1>(previous_lower) + 1;
        return bv_out_degree_select(new_node_b) + 1;
      } else {
        return 0;
      }
    }
    
    /** Get to a predecessor of the current node returning a node of order new_k
     * */ 
    size_type reverse_follow(size_type b,
                             lcs_value_type new_k) const {
      assert(new_k <= k_max);
      edge_value_type c = get_c(b);
      size_type node_b = bv_out_degree_rank(b);
      size_type in_index;
      if (node_b == 0) {
        in_index = 0;
      }
      else {
        in_index = bv_in_degree_select(node_b)+1;
      }
      
      // How many edges labelled c before us
      size_type edge_rank = in_index - char_count[(*alphabet_rank)(c)];

      // Index of the edge we have taken
      size_type edge_index = wt_edges.select(edge_rank+1, c);

      return decrease_k(edge_index, new_k);
    }
    
    /** Decrease the order of the graph to new_k */
    size_type decrease_k(size_type b, lcs_value_type new_k) const {
      
      size_type node_b = bv_out_degree_rank(b);
      
      std::pair<bool, size_type> previous_lower =
          wt_lcs.template find_closest<0, 0>(node_b, new_k);
      if (std::get<0>(previous_lower)) {
        size_type new_node_b = std::get<1>(previous_lower) + 1;
        return bv_out_degree_select(new_node_b) + 1;
      } else {
        return 0;
      }
    }
    
    /** Increment by 1 the order of the graph such that the first added
     *  character on the left is the n_distinct lower-th availlable */
    size_type incr_k(size_type b,
                     lcs_value_type k,
                     size_type n_distinct) const {
      if (n_distinct == 0) {
        return b;
      }
      else {
        size_type node_b = bv_out_degree_rank(b);
        size_type new_node_b =  wt_lcs.select(wt_lcs.rank(node_b, k)
                                               + n_distinct,
                                              k) + 1;
        return bv_out_degree_select(new_node_b) + 1;
      }
    }
    
    /** Find the number of distinct characters lower than c exiting the node */
    size_type get_n_distinct(size_type b,
                             lcs_value_type k,
                             edge_value_type c) const {
      size_type e = get_e(b, k);
      // TODO most of these vectors are not used, could used a simpler version
      // of intervals_symbols
      std::vector<edge_value_type> cs((*alphabet_rank)(alphabet->size()));
      std::vector<size_type> cs_rank_i((*alphabet_rank)(alphabet->size()));
      std::vector<size_type> cs_rank_j((*alphabet_rank)(alphabet->size()));
      
      size_type n_differents;
      
      wt_edges.interval_symbols(b, e, n_differents, cs, cs_rank_i, cs_rank_j);
      
      int cb = 0;
      int ce = n_differents;
      while (ce - cb > 1) {
        int cm = (cb + ce) / 2;
        if (cs[cm] <= c) {
          cb = cm;
        } else {
          ce = cm;
        }
      }
      
      if (cs[cb] == c) {
        return cb;
      } else {
        return cb + 1;
      }
    }
    
    /** Fill vector out_edges with the sorted availlable edges and return their
     *  number */
    size_type edges_list(std::vector<edge_value_type> &out_edges,
                         size_type b,
                         lcs_value_type k) const {
      size_type e = get_e(b, k);
      // Same as for get_n_lower
      std::vector<size_type> cs_rank_i((*alphabet_rank)(alphabet->size()));
      std::vector<size_type> cs_rank_j((*alphabet_rank)(alphabet->size()));
      size_type n_differents;
      wt_edges.interval_symbols(b, e, n_differents, out_edges, cs_rank_i, cs_rank_j);
      return n_differents;
    }
    
    /** Return an arbitrary edge leaving the current node */
    edge_value_type arbitrary_edge(size_type b) const {
      return wt_edges[b];
    }
    
    /** Return a unique index for each node of order k */
    size_type node_index(size_type b, lcs_value_type k) const {
      size_type node_b = bv_out_degree_rank(b);
      std::tuple<size_type, size_type> res =
        wt_lcs.lex_smaller_count(node_b, k);
      return std::get<1>(res);
    }
    
    size_type max_node_index(lcs_value_type k) const {
      std::tuple<size_type, size_type> res =
        wt_lcs.lex_smaller_count(wt_lcs.size(), k);
      return (std::get<1>(res) + 1);
    }
    
    /** Returns an approximation of the size of the structure */
    size_type byte_size() const {
      return (sdsl::size_in_bytes(char_count)
            + sdsl::size_in_bytes(wt_edges)
            + sdsl::size_in_bytes(wt_lcs)
            + sdsl::size_in_bytes(bv_in_degree)
            + sdsl::size_in_bytes(bv_in_degree_rank)
            + sdsl::size_in_bytes(bv_in_degree_select)
            + sdsl::size_in_bytes(bv_out_degree)
            + sdsl::size_in_bytes(bv_out_degree_rank)
            + sdsl::size_in_bytes(bv_out_degree_select));
    }
    
    size_type lcs_byte_size() const {
      return sdsl::size_in_bytes(wt_lcs);
    }
    
 };
#endif
