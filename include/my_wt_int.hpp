#ifndef ORDERED_WT_HPP
#define ORDERED_WT_HPP
#include <sdsl/wavelet_trees.hpp>
#include <sdsl/bit_vectors.hpp>

#include <iostream>

class OrderedWt: public sdsl::wt_int<> {
  
  //static_assert(lex_ordered);
  
  public:
    using sdsl::wt_int<>::wt_int;
    
    /** Find the maximum j such that j < i and vec[j] < c where vec is the
     *  original vector.
     *  If there are none, return -1 (max value_type) */ 
    size_type previous_lower(size_type i, value_type c) const {
      assert(i <= size());
      if (((1ULL)<<(m_max_level))<=c) { // c is greater than any symbol in wt
        return i-1;
      }
      size_type res = 0;
      size_type res_at_step_found = res;
      int step_found = -1;
      sdsl::int_vector<64> m_path_off(max_level);
      sdsl::int_vector<64> m_path_rank_off(max_level);

      size_type offset = 0;
      uint64_t mask = (1ULL) << (m_max_level-1);
      size_type node_size = m_size;
      
      for (uint32_t k=0; k < m_max_level; ++k) {
        if (res >= i) {
          break;
        }
        size_type ones_before_o   = m_tree_rank(offset);
        size_type ones_before_i   = m_tree_rank(offset + i) - ones_before_o;
        size_type ones_before_res = m_tree_rank(offset + res) - ones_before_o;
        size_type ones_before_end = m_tree_rank(offset + node_size) - ones_before_o;
        m_path_off[k] = offset;
        m_path_rank_off[k] = ones_before_o;
        if (c & mask) {
          // select last zero and continue the search to the right
          if (res - ones_before_res < i - ones_before_i) {
            // there is a zero between res and i
            res = m_tree_select0(offset-ones_before_o + i-ones_before_i) - offset;
            ones_before_res = m_tree_rank(offset + res) - ones_before_o;
            res_at_step_found = res;
            step_found = k;
          }
          res = ones_before_res;
          offset += (node_size - ones_before_end);
          node_size = ones_before_end;
          i = ones_before_i;
        } else {
          // continue the search to the left
          res = res - ones_before_res;
          node_size = (node_size - ones_before_end);
          i = (i-ones_before_i);
        }
        offset += m_size;
        mask >>= 1;
      }

      res = res_at_step_found;
      mask = 1ULL << (m_max_level - step_found);
      if (step_found != -1) {
        for (uint32_t k = step_found; k > 0; --k) {
          offset = m_path_off[k-1];
          size_type ones_before_o = m_path_rank_off[k-1];
          if (c & mask) { // right child => search res'th
            res = m_tree_select1(ones_before_o + res + 1) - offset;
          } else { // left child => search res'th zero
            res = m_tree_select0(offset - ones_before_o + res + 1) - offset;
          }
          mask <<= 1;
        }
        return res;
      }
      else {
        return -1;
      }
    }
    
    
    
    size_type next_lower(size_type i, value_type c) const {
      assert(i < size());
      if (((1ULL)<<(m_max_level))<=c) { // c is greater than any symbol in wt
        return i+1;
      }
      size_type res = size();
      size_type res_at_step_found = res;
      int step_found = -1;
      sdsl::int_vector<64> m_path_off(max_level);
      sdsl::int_vector<64> m_path_rank_off(max_level);

      size_type offset = 0;
      uint64_t mask = (1ULL) << (m_max_level-1);
      size_type node_size = m_size;
      
      for (uint32_t k=0; k < m_max_level; ++k) {
        if (res <= i) {
          break;
        }
        size_type ones_before_o   = m_tree_rank(offset);
        size_type ones_before_i   = m_tree_rank(offset + i) - ones_before_o;
        size_type ones_before_res = m_tree_rank(offset + res) - ones_before_o;
        size_type ones_before_end = m_tree_rank(offset + node_size) - ones_before_o;
        m_path_off[k] = offset;
        m_path_rank_off[k] = ones_before_o;
        if (c & mask) {
          // select next zero and continue the search to the right
          if (ones_before_res - res < ones_before_i - i) {
            // there is a zero between res and i
            res = m_tree_select0(offset-ones_before_o + i-ones_before_i + 1) - offset;
            ones_before_res = m_tree_rank(offset + res) - ones_before_o;
            res_at_step_found = res;
            step_found = k;
          }
          res = ones_before_res;
          offset += (node_size - ones_before_end);
          node_size = ones_before_end;
          i = ones_before_i;
        } else {
          // continue the search to the left
          res = res - ones_before_res;
          node_size = (node_size - ones_before_end);
          i = (i-ones_before_i);
        }
        offset += m_size;
        mask >>= 1;
      }

      res = res_at_step_found;
      mask = 1ULL << (m_max_level - step_found);
      if (step_found != -1) {
        for (uint32_t k = step_found; k > 0; --k) {
          offset = m_path_off[k-1];
          size_type ones_before_o = m_path_rank_off[k-1];
          if (c & mask) { // right child => search res'th
            res = m_tree_select1(ones_before_o + res + 1) - offset;
          } else { // left child => search res'th zero
            res = m_tree_select0(offset - ones_before_o + res + 1) - offset;
          }
          mask <<= 1;
        }
        return res;
      }
      else {
        return -1;
      }
    }

};



#endif
