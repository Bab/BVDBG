#ifndef BVDBG_WRAPPER
#define BVDBG_WRAPPER

#include "node.hpp"
#include "bvdbg.hpp"
#include <assert.h>
#include <vector>
#include <stack>

/** A wrapper class for abstracting a bvdbg into a strongly connected graph */
template<class t_bvdbg = bvdbg<>>
class bvdbg_wrapper {
  public:
    typedef t_bvdbg                              bvdbg_type;
    typedef typename bvdbg_type::size_type       size_type;
    typedef typename bvdbg_type::edge_value_type edge_value_type;
    typedef typename bvdbg_type::lcs_value_type  lcs_value_type;
    typedef typename bvdbg_type::node_type       node_type;
    typedef small_node<node_type>                small_node_type;
  
  private:
    /** A pointer to the underlying bvdbg */
    const bvdbg_type *bg;
    
    /** The order for which the wrapper is made */
    lcs_value_type k;
    
    /** A vector allowing to get back the nodes from there indexes */
    std::vector<small_node_type> node_vec;
    
    /** A vector telling which nodes are part of a strongly connected component
     * */
    std::vector<bool> scc;
    
    /** The index of a node of the graph that don't have any $ character in it
     * */
    size_type r;
  
  public:
    bvdbg_wrapper() {}
    
    bvdbg_wrapper(const bvdbg_type &bg0, lcs_value_type k0):
        bg(&bg0), k(k0) {
      size_type n = bg->max_node_index(k);
      node_vec = std::vector<small_node_type>(n);
      scc = std::vector<bool>(n, false);
      
      // First, find a node of order k without \0 or $ in its pattern
      node_type v = bg->empty();
      std::vector<edge_value_type> edges(bg->alphabet_size());
      while(v.k < k) {
        int ord = bg->template edges<1>(edges, v);
        int i = 0;
        while(i < ord && (edges[i] == 0 || edges[i] == '$')) {
          ++i;
        }
        assert(i < ord);
        v = bg->template add<1>(v, edges[i]);
      }
      r = bg->node_index(v);
      
      // Then do a first DFS, updating node_vec
      std::stack<small_node_type> st;
      std::vector<bool> visited0(n, false);
      st.push(small_node_type(v));
      while(!st.empty()) {
        v = st.top().full(k);
        st.pop();
        size_type idv = bg->node_index(v);
        if (!visited0[idv]) {
          visited0[idv] = true;
          node_vec[idv] = small_node_type(v);
          int ord = bg->template edges<0>(edges, v);
          node_type v0 = bg->template del<1>(v);
          for (int i = 0; i < ord; ++i) {
            if (edges[i] != 0 && edges[i] != '$') {
              node_type w = bg->template add<0>(v0, edges[i]);
              st.push(small_node_type(w));
            }
          }
        }
      }
      
      // Do a second DFS on the reverse of the graph
      st.push(small_node_type(node_vec[r]));
      std::vector<bool> visited1(n, false);
      while(!st.empty()) {
        v = st.top().full(k);
        st.pop();
        size_type idv = bg->node_index(v);
        if (!visited1[idv]) {
          visited1[idv] = true;
          node_vec[idv] = small_node_type(v);
          int ord = bg->template edges<1>(edges, v);
          node_type v0 = bg->template del<0>(v);
          for (int i = 0; i < ord; ++i) {
            if (edges[i] != 0 && edges[i] != '$') {
              node_type w = bg->template add<1>(v0, edges[i]);
              st.push(small_node_type(w));
            }
          }
        }
      }
      
      // scc = visited0 && visited1
      for (int i = 0; i < n; ++i) {
        scc[i] = visited0[i] && visited1[i];
      }
    }
    
    /** return a node that doesn't contain any 0 in its pattern */
    size_type get_r() const {
      return r;
    }
    
    /** return the maximum index of any node */
    size_type max_index() const {
      return node_vec.size();
    }
    
    /** return the maximum order of a node */
    size_type max_order() const {
      return bg->alphabet_size();
    }
    
    node_type orig_node(size_type idx) const {
      assert(idx < max_index());
      return node_vec[idx].full(k);
    }
    
    size_type wrapper_node(node_type v) const {
      assert (v.k == k);
      return bg->node_index(v);
    }
    
    template<bool right>
    edge_value_type get_last_char(size_type idx) const {
      return bg->template get_last_char<right>(orig_node(idx));
    }
    
    std::string get_pattern(size_type idx) const {
      return bg->get_pattern(orig_node(idx));
    }
    
    /** Fill vector next with the successors of idx in the graph.
     *  If right is false, operate on its reverse */
    template<bool right>
    size_type next_nodes(std::vector<size_type> &next, size_type idx) const {
      assert(idx < max_index() && scc[idx]);
      node_type v = orig_node(idx);
      std::vector<edge_value_type> edges(bg->alphabet_size());
      int ord = bg->template edges<right>(edges, v);
      size_type i0 = 0;
      for (size_type i = 0; i < ord; ++i) {
        edge_value_type c = edges[i];
        if (c != 0 && c != '$') {
          // We don't want 0 or $ in a node's pattern
          size_type idy = wrapper_node(
            bg->template add<right>(bg->template del<!right>(v), c)
          );
          if (scc[idy]) {
            // We want the nodes to be part of the strongly connected component
            next[i0] = idy;
            ++i0;
          }
        }
      }
      return i0;
    }
    
    /** Returns true iff idx --> idy in the graph */
    bool is_edge(size_type idx, size_type idy) const {
      assert(idx < max_index() && idy < max_index() && scc[idx] && scc[idy]);
      node_type u = orig_node(idx);
      edge_value_type c = get_last_char<1>(idy);
      if (!(bg->template check<1>(u, c))) {
        return false;
      }
      node_type v = bg->template add<1>(bg->template del<0>(u), c);
      size_type idy2 = wrapper_node(v);
      return (idy == idy2);
    }
};
#endif
