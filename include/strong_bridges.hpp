#ifndef STRONG_BRIDGES_HPP
#define STRONG_BRIDGES_HPP

#include "dominator.hpp"
#include "dfs.hpp"
#include "bvdbg_wrapper.hpp"

#include <assert.h>
#include <tuple>
#include <set>

/** Returns true if the edge arriving to v in the spanning tree of the dfs is
 *  an edge dominator in the flowgraph of g */
template<class t_graph = bvdbg_wrapper<>, bool right>
bool is_edge_dominator (const t_graph &g,
                        const dfs<t_graph, right> &df,
                        const dominator<t_graph, right> &dom,
                        typename t_graph::size_type v) {
  using size_type = typename t_graph::size_type;
  size_type p = df.parent(v);
  std::vector<size_type> in_edges(g.max_order());
  int order = g.template next_nodes<!right>(in_edges, v);
  for (int i = 0; i < order; ++i) {
    size_type w = in_edges[i];
    if (w == p) {
      continue;
    }
    if (df.pre(w) < df.pre(v) ||
        df.post(w) > df.post(v) ||
        !dom.dominate(v, w)) {
      // w, v is a forward or cross edge
      return false;
    }
  }
  return true;
}

/** Returns the set of strong bridges of the graph g */
template<class t_graph = bvdbg_wrapper<>>
std::set<std::pair<typename t_graph::size_type, typename t_graph::size_type>>
strong_bridges(const t_graph &g) {
  using size_type = typename t_graph::size_type;
  std::set<std::pair<size_type, size_type>> res;
  
  dfs<t_graph, 0> df0(g);
  dominator<t_graph, 0> dom0(g, df0);

  size_type n = df0.n_nodes();
  for (size_type i = 1; i < n; ++i) {
    size_type v = df0.i_pre(i);
    if (is_edge_dominator(g, df0, dom0, v)) {
      res.insert({v, df0.parent(v)});
    }
  }
  dfs<t_graph, 1> df1(g);

  dominator<t_graph, 1> dom1(g, df1);

  for (size_type i = 1; i < n; ++i) {
    size_type v = df1.i_pre(i);
    if (is_edge_dominator(g, df1, dom1, v)) {
      res.insert({df1.parent(v), v});
    }
  }
  return res;
}

#endif
