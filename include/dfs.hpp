#ifndef DFS_HPP
#define DFS_HPP

#include <tuple>
#include <vector>
#include <assert.h>
#include "bvdbg_wrapper.hpp"

/** A class that associate the preorder and postorder of a depth first search
 *  to each node of the given graph; if right is false, does the same thing for
 *  the reverse graph*/
template<class t_graph = bvdbg_wrapper<>, bool right = true>
class dfs {
  public:
    typedef t_graph                              graph_type;
    typedef typename graph_type::size_type       size_type;
    
  private:
    const graph_type *g;
    /** pre_order associate to each node its pre_order */
    std::vector<size_type> pre_order;
    
    /** node_vec associate to each pre_order its node */
    std::vector<size_type> node_vec;
    
    /** post_order associate to each node its post_order */
    std::vector<size_type> post_order;
  
  public:
    dfs() {}
  
    dfs(const graph_type &g0): g(&g0) {
      size_type n = g->max_index();
      node_vec = std::vector<size_type>(n, -1);
      pre_order = std::vector<size_type>(n, -1);
      post_order = std::vector<size_type>(n, -1);
      
      // Stack for the depth-first search
      std::stack<size_type> st;
      
      size_type v = g->get_r();
      st.push(v);
      
      std::vector<size_type> next(g->max_order(), -1);
      size_type count = 0;
      size_type count_post = 0;
      while (!st.empty()) {
        v = st.top();
        if (pre_order[v] == -1) {
          pre_order[v] = count;
          node_vec[count] = v;
          ++count;
          
          int order = g->template next_nodes<right>(next, v);
          for (int i = 0; i < order; ++i) {
            if (pre_order[next[i]] == -1) {
              st.push(next[i]);
            }
          }
        } else {
          if (post_order[v] == -1) {
            post_order[v] = count_post;
            ++count_post;
          }
          st.pop();
        }
      }
      assert(count == count_post);
      node_vec.resize(count);
    }
    
    size_type n_nodes() const {
      return node_vec.size();
    }
    
    size_type pre(size_type idx) const {
      assert(idx < pre_order.size());
      return pre_order[idx];
    }
    
    size_type post(size_type idx) const {
      assert(idx < post_order.size());
      return post_order[idx];
    }
    
    size_type i_pre(size_type p) const {
      assert(p < node_vec.size());
      return node_vec[p];
    }
    
    size_type parent(size_type v) const {
      std::vector<size_type> next(g->max_order(), -1);
      int order = g->template next_nodes<!right>(next, v);
      size_type res = i_pre(0);
      for (int i = 0; i < order; ++i) {
        if (pre(res) < pre(next[i]) && pre(next[i]) < pre(v)) {
          res = next[i];
        }
      }
      return res;
    }
          
          
};
#endif
