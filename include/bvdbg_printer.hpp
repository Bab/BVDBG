#ifndef BVDBG_PRINTER_HPP
#define BVDBG_PRINTER_HPP

#include <iostream>
#include <string>

template <class t_hbv>
void hbv_printer(std::ostream &os, const t_hbv hbv) {
  typename t_hbv::wt_edges_type edges = hbv.get_edges();
  for (typename t_hbv::size_type i = 0; i < edges.size(); ++i) {
    typename t_hbv::edge_value_type c = edges[i];
    if (c == 0) {
      os << '$';
    } else {
      os << edges[i];
    }
  }
  os << std::endl;

  typename t_hbv::bitvector_type out_degree = hbv.get_out_degree();
  for (typename t_hbv::size_type i = 0; i < out_degree.size(); ++i) {
    os << out_degree[i];
  }
  os << std::endl;
  
  typename t_hbv::bitvector_type in_degree = hbv.get_in_degree();
  for (typename t_hbv::size_type i = 0; i < in_degree.size(); ++i) {
    os << in_degree[i];
  }
  os << std::endl;
  
  typename t_hbv::wt_lcs_type lcs = hbv.get_lcs();
  for (typename t_hbv::size_type i = 0; i < lcs.size() - 1; ++i) {
    os << (int) lcs[i] << ", ";
  }
  os << (int) lcs[lcs.size() - 1] << std::endl;
}

/** Prints a BVDBG in os, usefull for debugging */
template <class t_bvdbg>
void bvdbg_printer(std::ostream &os, t_bvdbg bg) {
  typename t_bvdbg::half_bvdbg_type forward = bg.template get_hbv<1>();
  os << "------------Forward------------" << std::endl;
  hbv_printer(os, forward);
  typename t_bvdbg::half_bvdbg_type backward = bg.template get_hbv<0>();
  os << "------------Backward------------" << std::endl;
  hbv_printer(os, backward);
}

template <class t_node>
void node_printer(std::ostream &os, t_node v) {
  os << "k:          " << v.k << std::endl;
  os << "b_forward:  " << std::get<1>(v.b) << std::endl;
  os << "b_backward: " << std::get<0>(v.b) << std::endl;
}
#endif
