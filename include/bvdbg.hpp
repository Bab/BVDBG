#ifndef BVDBG_HPP
#define BVDBG_HPP

#include <tuple>
#include <string>
#include <assert.h>
#include "node.hpp"
#include "half_bvdbg.hpp"


/** A representation of a bidirectional varaible order de Bruijn graph */
template <class t_wt_edges  = sdsl::wt_hutu<>,
          class t_wt_lcs    = my_wt_hutu<>,
          class t_bitvector = sdsl::bit_vector,
          class t_rank      = typename t_bitvector::rank_1_type,
          class t_select    = typename t_bitvector::select_1_type
          >
class bvdbg{
  public:
    typedef t_wt_edges                          wt_edges_type;
    typedef t_wt_lcs                            wt_lcs_type;
    typedef typename wt_edges_type::size_type   size_type;
    typedef typename wt_edges_type::value_type  edge_value_type;
    typedef typename wt_lcs_type::value_type    lcs_value_type;
    typedef t_bitvector                         bitvector_type;
    typedef t_rank                              rank_type;
    typedef t_select                            select_type;
    typedef half_bvdbg<wt_edges_type,
                       wt_lcs_type,
                       bitvector_type,
                       rank_type,
                       select_type
                       > half_bvdbg_type;
  
    typedef node<size_type, lcs_value_type> node_type;
  
  private:
    /** max order of the bvdbg */
    lcs_value_type k_max;
    
    /** Contains the set of letters of the text */
    bitvector_type alphabet;
    rank_type      alphabet_rank;
    select_type    alphabet_select;
    
    /** Two half_bvdbgs representing both the forward and backward
     *  graph */
    std::pair<half_bvdbg_type, half_bvdbg_type> hbv;
    
  public:
    bvdbg() {}
  
    /** Constructor, taking the maximum order and an input text */
    bvdbg(lcs_value_type k, const std::string &input):
        k_max(k), alphabet(256) {
      size_type len = input.size();
      for (size_type i = 0; i < len; ++i) {
        alphabet[input[i]] = 1;
      }
      alphabet[0] = 1;
      alphabet_rank = rank_type(&alphabet);
      alphabet_select = select_type(&alphabet);
      
      std::get<0>(hbv) = half_bvdbg_type(k, input, &alphabet, &alphabet_rank,
                                         &alphabet_select);
      
      std::string reverse_input;
      for (size_type i = 0; i < len; ++i) {
        reverse_input.push_back(input[len-i-1]);
      }
      
      std::get<1>(hbv) = half_bvdbg_type(k, reverse_input, &alphabet,
                                         &alphabet_rank, &alphabet_select);
    }
    
    /** Const Accessors */
    lcs_value_type get_k_max() const {
      return k_max;
    }
    
    const bitvector_type& get_alphabet() const {
      return &alphabet;
    }
    
    template<bool right>
    const half_bvdbg_type &get_hbv() const {
      return std::get<right>(hbv);
    }
    
    /** Return an empty node */
    node_type empty() const {
      node_type res;
      std::get<0>(res.b) = 0;
      std::get<1>(res.b) = 0;
      res.k = 0;
      return res;
    }
    
    size_type alphabet_size() const {
      return alphabet_rank(alphabet.size());
    }
    
    std::vector<edge_value_type> alphabet_vector() const {
      std::vector<edge_value_type> res(alphabet_size(), 0);
      for (int i = 1; i <= alphabet_size(); ++i) {
        edge_value_type c_i = (edge_value_type) alphabet_select(i);
        res[i-1] = c_i;
      }
      return res;
    }
    
    /** Check if we can extend the pattern to the left/right with c */
    template<bool right>
    bool check(node_type v, edge_value_type c) const {
      return std::get<right>(hbv).check_edge(std::get<right>(v.b), v.k, c);
    }

    /** Append c to the left/right of node v's pattern, require both that a c is
     *  availlable and that the order isn't maximal */
    template<bool right>
    node_type add(node_type v, edge_value_type c) const {
      assert(v.k < k_max);
      assert(check<right>(v, c));
      size_type n_distinct = std::get<right>(hbv).get_n_distinct(
                                                  std::get<right>(v.b), v.k, c);
      node_type w;
      std::get<right>(w.b) = std::get<right>(hbv).follow_edge(
                                                std::get<right>(v.b), c, v.k+1);
      std::get<!right>(w.b) = std::get<!right>(hbv).incr_k(
                                        std::get<!right>(v.b), v.k, n_distinct);
      w.k = v.k + 1;
      return w;
    }
    
    /** Delete a character to the left/right of the current pattern,
     * require the pattern not to be empty */
    template<bool right>
    node_type del(node_type v) const {
      assert(v.k > 0);
      node_type w;
      std::get<right>(w.b) = std::get<right>(hbv).reverse_follow(
                                                   std::get<right>(v.b), v.k-1);
      std::get<!right>(w.b) = std::get<!right>(hbv).decrease_k(
                                                  std::get<!right>(v.b), v.k-1);
      w.k = v.k - 1;
      return w;
    }
    
    /** Fill vector in_edges with the sorted list of in_edges' label of node v
     * */
    template<bool right>
    int edges(std::vector<edge_value_type> &in_edges, node_type v) const {
      return std::get<right>(hbv).edges_list(in_edges,
                                             std::get<right>(v.b), v.k);
    }
    
    /** Returns a unique index for a node v. Is bijective when k is fixed
     *  can be used as a perfect hash function */
    size_type node_index(const node_type v) const {
      return std::get<1>(hbv).node_index(std::get<1>(v.b), v.k);
    }
    
    /** Returns the gretest value returned by node_index + 1*/
    size_type max_node_index(lcs_value_type k) const {
      return std::get<1>(hbv).max_node_index(k);
    }
    
    /** Returns the last or the first character of the node v depending of the
     * value of right */
    template<bool right>
    edge_value_type get_last_char(node_type v) const {
      return std::get<right>(hbv).get_c(std::get<right>(v.b));
    }
    
    /** Returns as a string the full pattern of a node */
    std::string get_pattern(node_type v) const {
      std::string res;
      int k = v.k;
      for (int i = 0; i < k; ++i) {
        edge_value_type c = get_last_char<0>(v);
        if (c == 0) {
          c = '$';
        }
        res.push_back(c);
        v = del<0>(v);
      }
      return res;
    }
    
    /** Returns an approximation of the size of the bvdbg */
    size_type byte_size() const {
      return (std::get<0>(hbv).byte_size()
            + std::get<1>(hbv).byte_size()
            + sdsl::size_in_bytes(alphabet)
            + sdsl::size_in_bytes(alphabet_rank)
            + sdsl::size_in_bytes(alphabet_select));
    }
    
    /** Returns the size of the LCS in the BVDBG */
    size_type lcs_byte_size() const {
      return (std::get<0>(hbv).lcs_byte_size()
            + std::get<1>(hbv).lcs_byte_size());
    }
    
};

#endif
