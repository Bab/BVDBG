\documentclass[11pt]{beamer}
\usetheme{Rochester}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{tabularx}
\usepackage{multirow}
\usepackage{graphicx}
\usepackage{tikz}
\usepackage{pgfplots}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage[squaren,Gray]{SIunits}
\usepackage{caption}

\definecolor{Green}{RGB}{30,124,37}
\setbeamercolor{structure}{fg=Green}

\setbeamertemplate{footline}{\hspace*{\fill}\insertframenumber/\inserttotalframenumber\hspace*{0.3cm}}
\setbeamerfont{footline}{series=\bfseries,size=\fontsize{8}{12}\selectfont}
\setbeamertemplate{navigation symbols}{}

\graphicspath{{pic/}}

% For not numbering backup slides
\newcommand{\beginbackup}{
   \newcounter{framenumbervorappendix}
   \setcounter{framenumbervorappendix}{\value{framenumber}}
}
\newcommand{\backupend}{
   \addtocounter{framenumbervorappendix}{-\value{framenumber}}
   \addtocounter{framenumber}{\value{framenumbervorappendix}} 
}


\newcommand*\fish[1][black]{\, %
\raisebox{-3pt} {\tikz[scale = 0.6]{%
\draw[line width = 0.7pt, color=#1 ] (13pt, 5.15pt) .. controls (11pt, -3pt) and (3.5pt, 3pt) .. (0pt, 8.5pt) .. controls (0.8pt, 5pt) .. (0pt, 1.5pt) .. controls (3.5pt, 7pt) and (11pt, 13pt) .. (13pt, 4.85pt);
\draw[color = #1, fill=#1] (10.5pt, 6.5pt) circle[radius = 0.3pt];
}}
}

\title{De Bruijn Graph Generalisations and Applications}
\author{Bastien Thomas \and Veli M\"akinen \and Simon Puglisi \\ Leena Salmela}
\date{}

\begin{document}
\frame{
\maketitle
}


\section{Introduction}

\frame{
\frametitle{Introduction}
\begin{columns}
  \begin{column}{0.4\textwidth}
    \begin{itemize}
      \item Genomes are commonly several Gigabytes long
      \item We need to make complex operations on them
      \item We need adapted data structures
    \end{itemize}
  \end{column}
  \begin{column}{0.6\textwidth}
    \begin{center}
    \includegraphics[width = \textwidth]{Genome_Sizes.png}
    
    The size of the genome of different biological classes (logarithmic scale)
    \tiny{(\url{https://en.wikipedia.org/wiki/Genome_size})}
    \end{center}
  \end{column}
\end{columns}
}

\begin{frame}
\frametitle{Overview} 
\tableofcontents
\end{frame}

\section{Preliminaries}

\subsection{Bitvectors}
\frame{
\frametitle{Bitvectors}
\begin{columns}
  \begin{column}{0.5\textwidth}
A static array of $n$ bits that supports the queries:
\begin{itemize}
\item<2-> $\mbox{\texttt{rank}}_0(i)$: The number of $0$s before $i$
\item<3-> $\mbox{\texttt{rank}}_1(i)$: The number of $1$s before $i$
\item<4-> $\mbox{\texttt{select}}_0(i)$: The position of the $i$-th $0$
\item<5-> $\mbox{\texttt{select}}_1(i)$: The position of the $i$-th $1$
\end{itemize}
  \end{column}
  \begin{column}{0.5\textwidth}
    \begin{center}
    \begin{tabular}{|c|c|c|c|c|c|c|}
    \hline
    0 & 1 & 2 & 3 & 4 & 5 & 6\\
    \hline
    0 & 0 & 1 & 0 & 1 & 1 & 1\\
    \hline
    \end{tabular}
    \end{center}
    \begin{itemize}
      \item<2-> $\mbox{\texttt{rank}}_0(4) = 3$
      \item<3-> $\mbox{\texttt{rank}}_1(4) = 1$
      \item<4-> $\mbox{\texttt{select}}_0(3) = 3$
      \item<5-> $\mbox{\texttt{select}}_1(3) = 5$
    \end{itemize}
  \end{column}
\end{columns}
\onslide<6->{
\begin{block}{Complexity}
All these queries require $\mathcal{O}(1)$ time, with an additional $o(n)$ bits.
\end{block}}
}

\subsection{Wavelet trees}
\frame{
\frametitle{Wavelet trees (WT)}
\begin{columns}
  \begin{column}{0.5\textwidth}
    A generalisation of bitvectors to a string $s$ over any alphabet $\Sigma$.
    \begin{enumerate}
    \item<2-> Divide $\Sigma$ into $\Sigma_0$ and $\Sigma_1$
    \item<3-> Build a bivector $b$: $b[i] = 0$ iff $s[i] \in \Sigma_0$
    \item<4-> Recursively build WT for $s_0 = s\left[i; b[i] = 0\right]$ and $s_1 = s\left[i; b[i] = 1\right]$
    \item<5-> Stop when $\left| \Sigma \right| = 1$
    \end{enumerate}
  \end{column}
  \begin{column}{0.5\textwidth}
    \begin{center}
    \resizebox{\textwidth}{!}{
    \begin{tikzpicture}[scale = 0.0065\textwidth]
    \tikzstyle{node}=[align = center]
    
    \node[node] (s) at (4, 6) {
    \begin{tabular}{l@{$ = $}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}}
    $\Sigma$ &\multicolumn{11}{@{}l@{}}{$\{a, b, c, d, r\}$}\\
    $s$ & $a$ & $b$ & $r$ & $a$ & $c$ & $a$ & $d$ & $a$ & $b$ & $r$ & $a$\\
    $b$ & \color{blue}{$0$} & \color{blue}{$0$} & \color{blue}{$1$} & \color{blue}{$0$} & \color{blue}{$0$} & \color{blue}{$0$} & \color{blue}{$1$} & \color{blue}{$0$} & \color{blue}{$0$} & \color{blue}{$1$} & \color{blue}{$0$}
    \end{tabular}
    };
    \node[node] (s0) at (2.5, 4) {
    \begin{tabular}{l@{$ = $}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}}
    $\Sigma_0$ & \multicolumn{8}{@{}l}{$\{a, b, c\}$}\\
    $s_0$ & $a$ & $b$ & $a$ & $c$ & $a$ & $a$ & $b$ & $a$\\
    $b_0$ & \color{blue}{$0$} & \color{blue}{$1$} & \color{blue}{$0$} & \color{blue}{$1$} & \color{blue}{$0$} & \color{blue}{$0$} & \color{blue}{$1$} & \color{blue}{$0$}
    \end{tabular}
    };
    \node[node] (s1) at (5.5, 4) {
    \begin{tabular}{l@{$ = $}c@{}c@{}c@{}c}
    $\Sigma_1$ & \multicolumn{4}{@{}l@{}}{$\{d, r\}$}\\
    $s_1$ & $r$ & $d$ & $r$&\\
    $b_1$ & \color{blue}{$0$} & \color{blue}{$1$} & \color{blue}{$0$}&
    \end{tabular}
    };
    \node[node] (s00) at (1.75, 2.5) {\color{blue}{$a$}};
    \node[node] (s01) at (3.25, 2) {
    \begin{tabular}{l@{$ = $}c@{}c@{}c@{}c}
    $\Sigma_{01}$ & \multicolumn{4}{@{}l@{}}{$\{b, c\}$}\\
    $s_{01}$ & $b$ & $c$ & $b$&\\
    $b_{01}$ & \color{blue}{$0$} & \color{blue}{$1$} & \color{blue}{$0$}&
    \end{tabular}
    };
    \node[node] (s10) at (4.75, 2.5) {\color{blue}{$d$}};
    \node[node] (s11) at (6.25, 2.5) {\color{blue}{$r$}};
    \node[node] (s010) at (2.75, 0.5) {\color{blue}{$b$}};
    \node[node] (s011) at (3.75, 0.5) {\color{blue}{$c$}};
    
    \draw (s) -- (s0);
    \draw (s) -- (s1);
    \draw (s0) -- (s00);
    \draw (s0) -- (s01);
    \draw (s01) -- (s010);
    \draw (s01) -- (s011);
    \draw (s1) -- (s10);
    \draw (s1) -- (s11);
    \end{tikzpicture}}
    \end{center}
  \end{column}
\end{columns}
}

\frame{
\frametitle{Wavelet trees (WT)}
\begin{columns}
  \begin{column}{0.6\textwidth}
    A WT supports the following queries:
    \begin{itemize}
    \item<2-> $\mbox{\texttt{rank}}(i, c)$: How many $c$ before $i$
    \item<3-> $\mbox{\texttt{select}}(i, c)$: Position of the $i$-th $c$
    \end{itemize}
  \end{column}
  \begin{column}{0.4\textwidth}
    \begin{center}
    \begin{tabular}{@{\,}l@{\,}l@{\,}l@{\,}l@{\,}l@{\,}l@{\,}l@{\,}l@{\,}l@{\,}l@{\,}l}
    $0$ & $1$ & $2$ & $3$ & $4$ & $5$ & $6$ & $7$ & $8$ & $9$ &$10$\\
    \hline
    $a$ & $b$ & $r$ & $a$ & $c$ & $a$ & $d$ & $a$ & $b$ & $r$ & $a$\\
    \end{tabular}
    \end{center}
    
    \begin{itemize}
    \item<2-> $\mbox{\texttt{rank}}(6, a) = 3$
    \item<3-> $\mbox{\texttt{select}}(2, b) = 8$
    \end{itemize}
  \end{column}
\end{columns}
\onslide<4->{
\begin{block}{Complexity}
All these queries require $\mathcal{O}(\log\left|\Sigma\right|)$ time, with $\mathcal{O}(n\log\left|\Sigma\right|)$ bits.
\end{block}

If the characters are ordered, it can support additional queries.}
}

\subsection{Burrows-Wheeler Transform}
\frame{
\frametitle{Burrows-Wheeler Transform (BWT)}
\begin{columns}
  \begin{column}{0.55\textwidth}
    A reversible transformation of a sequence $s \in \Sigma^n$.
    
    $\mbox{\texttt{BWT}}(s) \in \left(\Sigma \cup \fish\right)^{n+1}$, $\fish < \min(\Sigma)$.
    \begin{enumerate}
    \item<2-> Append $\fish$ at the end of $s$
    \item<3-> Construct and sort all the cyclic permutations of $s\fish$ in a matrix $M$
    \item<4-> $\mbox{\texttt{BWT}}(s)$ is the last column of $M$
    \end{enumerate}
  \end{column}
  \begin{column}{0.45\textwidth}
    \begin{center}
    \begin{tabular}{c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c}%
    $\fish$ & $a$ & $b$ & $r$ & $a$ & $c$ & $a$ & $d$ & $a$ & $b$ & $r$ & \color{blue}{$a$} \\
    $a$ & $\fish$ & $a$ & $b$ & $r$ & $a$ & $c$ & $a$ & $d$ & $a$ & $b$ & \color{blue}{$r$} \\
    $a$ & $b$ & $r$ & $a$ & $\fish$ & $a$ & $b$ & $r$ & $a$ & $c$ & $a$ & \color{blue}{$d$} \\
    $a$ & $b$ & $r$ & $a$ & $c$ & $a$ & $d$ & $a$ & $b$ & $r$ & $a$ & $\fish[blue]$ \\
    $a$ & $c$ & $a$ & $d$ & $a$ & $b$ & $r$ & $a$ & $\fish$ & $a$ & $b$ & \color{blue}{$r$} \\
    $a$ & $d$ & $a$ & $b$ & $r$ & $a$ & $\fish$ & $a$ & $b$ & $r$ & $a$ & \color{blue}{$c$}\\
    $b$ & $r$ & $a$ & $\fish$ & $a$ & $b$ & $r$ & $a$ & $c$ & $a$ & $d$ & \color{blue}{$a$} \\
    $b$ & $r$ & $a$ & $c$ & $a$ & $d$ & $a$ & $b$ & $r$ & $a$ & $\fish$ & \color{blue}{$a$} \\
    $c$ & $a$ & $d$ & $a$ & $b$ & $r$ & $a$ & $\fish$ & $a$ & $b$ & $r$ & \color{blue}{$a$} \\
    $d$ & $a$ & $b$ & $r$ & $a$ & $\fish$ & $a$ & $b$ & $r$ & $a$ & $c$ & \color{blue}{$a$} \\
    $r$ & $a$ & $\fish$ & $a$ & $b$ & $r$ & $a$ & $c$ & $a$ & $d$ & $a$ & \color{blue}{$b$} \\
    $r$ & $a$ & $c$ & $a$ & $d$ & $a$ & $b$ & $r$ & $a$ & $\fish$ & $a$ & \color{blue}{$b$} \\
    \end{tabular}
    \end{center}
  \end{column}
\end{columns}

%$\mbox{\texttt{BWT}}(abracadabra) = ard\fish rcaaaabb$
}


\subsection{FM-index}
\frame{
\frametitle{FM-index}
\begin{columns}
  \begin{column}{0.5\textwidth}
    If we know that $bra$ appears at the start of lines 6 and 7 of $M$, then:
    \begin{enumerate}
    \item<2-> $abra$ appears twice
    \item<3-> There is $1$ pattern starting with $a$ lower than $abra$
    \item<4-> Patterns starting with $a$ appears between $1$ and $5$
    \item<5-> $abra$ appears at lines 2 and 3
    \end{enumerate}
    \onslide<6->{
    We call this operation \texttt{add\_left}
    }
    
  \end{column}
  \begin{column}{0.5\textwidth}
    \begin{center}
    \begin{tabular}{l|c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c}%
    0 & $\fish$ & $a$ & $b$ & $r$ & $a$ & $c$ & $a$ & $d$ & $a$ & $b$ & $r$ & \color{blue}{$a$} \\
    1 & $a$ & $\fish$ & $a$ & $b$ & $r$ & $a$ & $c$ & $a$ & $d$ & $a$ & $b$ & \color{blue}{$r$} \\
    2 & $a$ & $b$ & $r$ & $a$ & $\fish$ & $a$ & $b$ & $r$ & $a$ & $c$ & $a$ & \color{blue}{$d$} \\
    3 & $a$ & $b$ & $r$ & $a$ & $c$ & $a$ & $d$ & $a$ & $b$ & $r$ & $a$ & $\fish[blue]$ \\
    4 & $a$ & $c$ & $a$ & $d$ & $a$ & $b$ & $r$ & $a$ & $\fish$ & $a$ & $b$ & \color{blue}{$r$} \\
    5 & $a$ & $d$ & $a$ & $b$ & $r$ & $a$ & $\fish$ & $a$ & $b$ & $r$ & $a$ & \color{blue}{$c$}\\
    6 & $b$ & $r$ & $a$ & $\fish$ & $a$ & $b$ & $r$ & $a$ & $c$ & $a$ & $d$ & \color{blue}{$a$} \\
    7 & $b$ & $r$ & $a$ & $c$ & $a$ & $d$ & $a$ & $b$ & $r$ & $a$ & $\fish$ & \color{blue}{$a$} \\
    8 & $c$ & $a$ & $d$ & $a$ & $b$ & $r$ & $a$ & $\fish$ & $a$ & $b$ & $r$ & \color{blue}{$a$} \\
    9 & $d$ & $a$ & $b$ & $r$ & $a$ & $\fish$ & $a$ & $b$ & $r$ & $a$ & $c$ & \color{blue}{$a$} \\
    10 & $r$ & $a$ & $\fish$ & $a$ & $b$ & $r$ & $a$ & $c$ & $a$ & $d$ & $a$ & \color{blue}{$b$} \\
    11 & $r$ & $a$ & $c$ & $a$ & $d$ & $a$ & $b$ & $r$ & $a$ & $\fish$ & $a$ & \color{blue}{$b$} \\
    \end{tabular}
    \end{center}
  \end{column}
\end{columns}

}

\frame{
\frametitle{FM-index}
An FM-index over a string $s$ is composed of the following:
\begin{description}
\item[\texttt{bwt}] wavelet tree storing $\mbox{\texttt{BWT}}(s)$
\item[$b, e$] the interval of the current pattern in \texttt{bwt}
\end{description}
\onslide<2->{
Then operation $\mbox{\texttt{add\_left}}(c)$ can be done in $\mathcal{O}\left(\log\left|\Sigma\right|\right)$:}
\begin{enumerate}
\item<3-> Get $n_c$: the number of $c$ that appear in $\mbox{\texttt{bwt}}([b, e])$ in $\mathcal{O}\left(\log\left|\Sigma\right|\right)$
\item<4-> Get $n_<$: the number of characters lower than $c$ in \texttt{bwt} either in $\mathcal{O}\left(\log\left|\Sigma\right|\right)$ or in $\mathcal{O}\left(1\right)$ if precomputed
\item<5-> Get $n_b$: the number of $c$ that appears before $b$ in $\mathcal{O}\left(\log\left|\Sigma\right|\right)$
\item<6-> $b \leftarrow n_< + n_b$, and $e \leftarrow b + n_c$
\end{enumerate}
\onslide<7->{
The structure takes $\mathcal{O}\left(n \log\left|\Sigma\right|\right)$ bits of space.
}
}

\frame{
\frametitle{FM-indexes usage}
\begin{columns}
\begin{column}{0.45\textwidth}
A sequencer produces millions of overlapping \textit{reads}.

We build an FM-index over these reads.
\end{column}
\begin{column}{0.55\textwidth}
\begin{center}
\begin{tikzpicture}
\node (dna) at (0, 0) {$ATTAGACTGATTA$};

\node[rectangle, draw] (seq) at (0, -1) {Sequencer};

\node (read1) at (-2, -2) {$TAGAC$};
\node (read2) at (-1, -2.5) {$ATTAG$};
\node (read3) at (0, -2) {$GATTA$};
\node (read4) at (1, -2.5) {$GACTG$};
\node (read5) at (2, -2) {$CTGAT$};

\node (conc) at (0, -3.5) {$TAGAC\fish ATTAG\fish GATTA\fish\dots$};

\node[rectangle, draw] (fmindex) at (0, -4.5) {FM-index};


\draw[->, thick, red] (dna) -- (seq);

\draw[->, thick, red] (seq) to [out = 180, in = 90] (read1);
\draw[->, thick, red] (seq) to [out = 225, in = 90] (read2);
\draw[->, thick, red] (seq) to [out = 270, in = 90] (read3);
\draw[->, thick, red] (seq) to [out = 315, in = 90] (read4);
\draw[->, thick, red] (seq) to [out = 0, in = 90] (read5);

\draw[->, thick, red] (read1) to [out = 270, in = 165] (conc);
\draw[->, thick, red] (read2) to [out = 270, in = 150] (conc);
\draw[->, thick, red] (read3) to [out = 270, in = 90] (conc);
\draw[->, thick, red] (read4) to [out = 270, in = 30] (conc);
\draw[->, thick, red] (read5) to [out = 270, in = 15] (conc);

\draw[->, thick, red] (conc) -- (fmindex);
\end{tikzpicture}
\end{center}
\end{column}
\end{columns}
\pause
\begin{block}{Consequence}
We'll never use the FM-index for long patterns.
\end{block}
}



\section{De Bruijn Graph}
\subsection{Motivation and Definition}
\frame{
\frametitle{Motivation}
FM-indexes can be too large.

Can we make them smaller if we only support patterns of a fixed length $k$?
\begin{block}{Query $\mbox{\texttt{add\_left\_delete\_right}}(c)$}
Given an already searched pattern $p = p_1 \dots p_k$, search for $c p_1 \dots p_{k-1}$
\end{block}
\pause
We are effectively looking for an \textit{order-$k$ de Bruijn graph}.
}

\frame{
\frametitle{Definition}
\begin{columns}
\begin{column}{0.6\textwidth}
The order-$k$ de Bruijn Graph of $s \in \Sigma^n$ is an oriented graph $G = (V, E)$ with:
\begin{itemize}
\item $p = p_1 \dots p_k \in V$ iff p appears in $s$
\item $(p, q) \in E$ iff $p_2 \dots p_k = q_1 \dots q_{k-1}$ and $p_1 \dots p_k q_k$ appear in $s$
\end{itemize}

We say that edge $(p, q)$ is labelled by $q_k$
\end{column}
\begin{column}{0.4\textwidth}
\begin{center}
\begin{tikzpicture}[scale = 1]
\tikzstyle{edge} = [->, red]
\node (abr) at (0, 0.5) {$abr$};
\node (bra) at (0, -0.5) {$bra$};
\node (rac) at (-1.1, -1) {$rac$};
\node (aca) at (-1.9, 0) {$aca$};
\node (cab) at (-1.1, 1) {$cab$};
\node (rad) at (1.1, -1) {$rad$};
\node (ada) at (1.9, 0) {$ada$};
\node (dab) at (1.1, 1) {$dab$};

\draw[edge] (abr) -> (bra);
\draw[edge] (bra) -> (rac);
\draw[edge] (rac) -> (aca);
\draw[edge] (aca) -> (cab);
\draw[edge] (cab) -> (abr);
\draw[edge] (bra) -> (rad);
\draw[edge] (rad) -> (ada);
\draw[edge] (ada) -> (dab);
\draw[edge] (dab) -> (abr);
\end{tikzpicture}
\end{center}
Order-$3$ De Bruin graph of $abracabradabra$
\end{column}
\end{columns}
}

\subsection{Implementation}
\frame{
\frametitle{Implementation}
\begin{columns}
\begin{column}{0.5\textwidth}
\begin{block}{Idea}
Remove the lines corresponding to patterns longer than $k$ in the FM-index
\end{block}
\onslide<2->{
Use a bitvector \texttt{Out} (for \textit{out-degree}) to tell when a pattern is preceded by different letters.

Use a bitvector \texttt{In} when a pattern can be accessed from several edges.
}
\end{column}
\begin{column}{0.5\textwidth}
    \begin{center}
    \begin{tabular}{|@{}c@{}|@{}c@{}c@{}c@{}|@{}c@{}|@{}c@{}|}%
    \hline
    Node & \multicolumn{3}{|@{}c@{}|}{Pattern} & Edges & Out\\
    \hline
    $0$ & $\fish$ & $a$ & $b$ & \color{blue}{$a$} & \color{blue}{1}\\
    $1$ & $a$ & $\fish $ & $a$ & \color{blue}{$r$} & \color{blue}{1}\\
    $2$ & $a$ & $b$ & $r$ & $\fish[blue]$ & \color{blue}{0}\\
     & & & & \color{blue}{$c$} & \color{blue}{0}\\
     & & & & \color{blue}{$d$} & \color{blue}{1}\\
    $3$ & $a$ & $c$ & $a$ & \color{blue}{$r$} & \color{blue}{1}\\
    $4$ & $a$ & $d$ & $a$ & \color{blue}{$r$} & \color{blue}{1}\\
    $5$ & $b$ & $r$ & $a$ & \color{blue}{$a$} & \color{blue}{1}\\
    $6$ & $c$ & $a$ & $b$ & \color{blue}{$a$} & \color{blue}{1}\\
    $7$ & $d$ & $a$ & $b$ & \color{blue}{$a$} & \color{blue}{1}\\
    $8$ & $r$ & $a$ & $\fish$ & \color{blue}{$b$} & \color{blue}{1}\\
    $9$ & $r$ & $a$ & $c$ & \color{blue}{$b$} & \color{blue}{1}\\
    $10$ & $r$ & $a$ & $d$ & \color{blue}{$b$} & \color{blue}{1}\\
    \hline
    \end{tabular}\quad%
    \begin{tabular}{|@{}c@{}|@{}c@{}|}%
    \hline
    Node & In\\
    \hline
    $0$ & \color{blue}{$1$}\\
    $1$ & \color{blue}{$1$}\\
    $2$ & \color{blue}{$1$}\\
    $3$ & \color{blue}{$1$}\\
    $4$ & \color{blue}{$1$}\\
    $5$ & \color{blue}{$0$}\\
     & \color{blue}{$0$}\\
     & \color{blue}{$1$}\\
    $6$ & \color{blue}{$1$}\\
    $7$ & \color{blue}{$1$}\\
    $8$ & \color{blue}{$1$}\\
    $9$ & \color{blue}{$1$}\\
    $10$ & \color{blue}{$1$}\\
    \hline
    \end{tabular}
    \end{center}
\end{column}
\end{columns}
}

\frame{
\frametitle{Implementation}
\begin{columns}
\begin{column}{0.6\textwidth}
\begin{block}{Properties}
\begin{itemize}
\item The in-edges are sorted by their labels
\item The $r$-th out-edge labelled $c$ is also the $r$-th in-edge labelled $c$
\end{itemize}
\end{block}
\end{column}
\begin{column}{0.4\textwidth}
\begin{center}
\begin{tikzpicture}[scale = 0.5]
\tikzstyle{edge} = [->, thick, red]

\node (fab0) at (0, 10) {$\fish ab$};
\node (afa0) at (0, 9) {$a\fish a$};
\node (abr0) at (0, 8) {$abr$};
\node (aca0) at (0, 7) {$aca$};
\node (ada0) at (0, 6) {$ada$};
\node (bra0) at (0, 5) {$bra$};
\node (cab0) at (0, 4) {$cab$};
\node (dab0) at (0, 3) {$dab$};
\node (raf0) at (0, 2) {$ra\fish$};
\node (rac0) at (0, 1) {$rac$};
\node (rad0) at (0, 0) {$rad$};

\node (fab1) at (4, 10) {$\fish ab$};
\node (afa1) at (4, 9) {$a\fish a$};
\node (abr1) at (4, 8) {$abr$};
\node (aca1) at (4, 7) {$aca$};
\node (ada1) at (4, 6) {$ada$};
\node (bra1) at (4, 5) {$bra$};
\node (cab1) at (4, 4) {$cab$};
\node (dab1) at (4, 3) {$dab$};
\node (raf1) at (4, 2) {$ra\fish$};
\node (rac1) at (4, 1) {$rac$};
\node (rad1) at (4, 0) {$rad$};

\draw[edge] (fab0) to[out = 0, in = 180] (afa1);
\draw[edge] (bra0) to[out = 0, in = 180] (abr1);
\draw[edge] (cab0) to[out = 0, in = 180] (aca1);
\draw[edge] (dab0) to[out = 0, in = 180] (ada1);
\end{tikzpicture}
\end{center}
The edges labelled $a$ of a de Bruijn Graph.
\end{column}
\end{columns}
}

\frame{
\frametitle{Implementation}
\begin{columns}
\begin{column}{0.5\textwidth}
If we know that $rac$ has index $9$,
\begin{enumerate}
\item<2-> The index of $rac$ in \texttt{Edges} is $\mbox{\texttt{Out.select}}_1(9) = 11$
\item<3-> $\mbox{\texttt{Edges}}[11] = b$ so $bra$ is a node
\item<4-> We compute $n_b = 1$ and $n_< = 5$ as if we were using an FM-index.
\item<5-> The $n_b + n_< = 6$-th in-edge enters $bra$.
\item<6-> The index of $bra$ is $\mbox{\texttt{In.rank}}_1(6) = 5$
\end{enumerate}
\end{column}
\begin{column}{0.5\textwidth}
    \begin{center}
    \begin{tabular}{|@{}c@{}|@{}c@{}c@{}c@{}|@{}c@{}|@{}c@{}|}%
    \hline
    Node & \multicolumn{3}{|@{}c@{}|}{Pattern} & Edges & Out\\
    \hline
    $0$ & $\fish$ & $a$ & $b$ & \color{blue}{$a$} & \color{blue}{1}\\
    $1$ & $a$ & $\fish $ & $a$ & \color{blue}{$r$} & \color{blue}{1}\\
    $2$ & $a$ & $b$ & $r$ & $\fish[blue]$ & \color{blue}{0}\\
     & & & & \color{blue}{$c$} & \color{blue}{0}\\
     & & & & \color{blue}{$d$} & \color{blue}{1}\\
    $3$ & $a$ & $c$ & $a$ & \color{blue}{$r$} & \color{blue}{1}\\
    $4$ & $a$ & $d$ & $a$ & \color{blue}{$r$} & \color{blue}{1}\\
    $5$ & $b$ & $r$ & $a$ & \color{blue}{$a$} & \color{blue}{1}\\
    $6$ & $c$ & $a$ & $b$ & \color{blue}{$a$} & \color{blue}{1}\\
    $7$ & $d$ & $a$ & $b$ & \color{blue}{$a$} & \color{blue}{1}\\
    $8$ & $r$ & $a$ & $\fish$ & \color{blue}{$b$} & \color{blue}{1}\\
    $9$ & $r$ & $a$ & $c$ & \color{blue}{$b$} & \color{blue}{1}\\
    $10$ & $r$ & $a$ & $d$ & \color{blue}{$b$} & \color{blue}{1}\\
    \hline
    \end{tabular}\quad%
    \begin{tabular}{|@{}c@{}|@{}c@{}|}%
    \hline
    Node & In\\
    \hline
    $0$ & \color{blue}{$1$}\\
    $1$ & \color{blue}{$1$}\\
    $2$ & \color{blue}{$1$}\\
    $3$ & \color{blue}{$1$}\\
    $4$ & \color{blue}{$1$}\\
    $5$ & \color{blue}{$0$}\\
     & \color{blue}{$0$}\\
     & \color{blue}{$1$}\\
    $6$ & \color{blue}{$1$}\\
    $7$ & \color{blue}{$1$}\\
    $8$ & \color{blue}{$1$}\\
    $9$ & \color{blue}{$1$}\\
    $10$ & \color{blue}{$1$}\\
    \hline
    \end{tabular}
    \end{center}
\end{column}
\end{columns}
}

\subsection{Bidirectional Variable-Order de Bruijn Graph (BVDBG)}
\frame{
\frametitle{Bidirectional Variable-Order de Bruijn Graph (BVDBG)}
\begin{itemize}
\item With a \textit{longest common prefix array} (\texttt{lcp}), we can search for patterns shorter than $k$, making the graph \textit{variable order}
\item By synchronising a de Bruijn Graph for a string $s$ and one for its reverse, we can make it \textit{bidirectional}
\end{itemize}
\pause
The final representation supports the queries:
\begin{itemize}
\item $\mbox{\texttt{add\_right}}(c)$ add $c$ to the right of the pattern
\item $\mbox{\texttt{add\_left}}(c)$ add $c$ to the left of the pattern
\item $\mbox{\texttt{del\_right}}()$ delete the last character of the pattern
\item $\mbox{\texttt{del\_left}}()$ delete the first character of the pattern
\end{itemize}
Up to a pattern of length $k$
}

\frame{
\frametitle{Final example}
\begin{center}
\begin{tabular}{|@{}c@{}|@{}c@{}c@{}c@{}|@{}c@{}|@{}c@{}|}
\hline
$\mbox{\texttt{lcp}}$ & \multicolumn{3}{|@{}c@{}|}{Pattern} & $\mbox{\texttt{edges}}$ & $\mbox{\texttt{out}}$ \\
\hline
\color{blue}{$0$} & $\fish$ & $a$ & $r$ & \color{blue}{$a$} & \color{blue}{$1$} \\
\color{blue}{$1$} & $a$ & $\fish$ & $a$ & \color{blue}{$b$} & \color{blue}{$1$} \\
\color{blue}{$1$} & $a$ & $c$ & $a$ & \color{blue}{$b$} & \color{blue}{$1$} \\
\color{blue}{$1$} & $a$ & $d$ & $a$ & \color{blue}{$b$} & \color{blue}{$1$} \\
\color{blue}{$0$} & $a$ & $r$ & $b$ & $\fish[blue]$ & \color{blue}{$0$} \\
& & & & \color{blue}{$c$} & \color{blue}{$0$} \\
& & & & \color{blue}{$d$} & \color{blue}{$1$} \\
\color{blue}{$2$} & $b$ & $a$ & $\fish$ & \color{blue}{$r$} & \color{blue}{$1$} \\
\color{blue}{$2$} & $b$ & $a$ & $c$ & \color{blue}{$r$} & \color{blue}{$1$} \\
\color{blue}{$0$} & $b$ & $a$ & $d$ & \color{blue}{$r$} & \color{blue}{$1$} \\
\color{blue}{$0$} & $c$ & $a$ & $r$ & \color{blue}{$a$} & \color{blue}{$1$} \\
\color{blue}{$0$} & $d$ & $a$ & $r$ & \color{blue}{$a$} & \color{blue}{$1$} \\
/ & $r$ & $b$ & $a$ & \color{blue}{$a$} & \color{blue}{$1$} \\
\hline
\end{tabular} %
\begin{tabular}{|@{}c@{}|}
\hline
$\mbox{\texttt{in}}$ \\
\hline
\color{blue}{$1$}\\
\color{blue}{$1$}\\
\color{blue}{$1$}\\
\color{blue}{$1$}\\
\color{blue}{$1$}\\
\color{blue}{$1$}\\
\color{blue}{$1$}\\
\color{blue}{$1$}\\
\color{blue}{$1$}\\
\color{blue}{$1$}\\
\color{blue}{$0$}\\
\color{blue}{$0$}\\
\color{blue}{$1$}\\
\hline
\end{tabular}\qquad%
\begin{tabular}{|@{}c@{}|@{}c@{}c@{}c@{}|@{}c@{}|@{}c@{}|}
\hline
$\mbox{\texttt{lcp}}^R$ & \multicolumn{3}{|@{}c@{}|}{Pattern} & $\mbox{\texttt{edges}}^R$ & $\mbox{\texttt{out}}^R$ \\
\hline
\color{blue}{$0$} & $\fish$ & $a$ & $b$ & \color{blue}{$a$} & \color{blue}{$1$} \\
\color{blue}{$1$} & $a$ & $\fish$ & $a$ & \color{blue}{$r$} & \color{blue}{$1$} \\
\color{blue}{$1$} & $a$ & $b$ & $r$ & $\fish[blue]$ & \color{blue}{$0$} \\
& & & & \color{blue}{$c$} & \color{blue}{$0$} \\
& & & & \color{blue}{$d$} & \color{blue}{$1$} \\
\color{blue}{$1$} & $a$ & $c$ & $a$ & \color{blue}{$r$} & \color{blue}{$1$} \\
\color{blue}{$0$} & $a$ & $d$ & $a$ & \color{blue}{$r$} & \color{blue}{$1$} \\
\color{blue}{$0$} & $b$ & $r$ & $a$ & \color{blue}{$a$} & \color{blue}{$1$} \\
\color{blue}{$0$} & $c$ & $a$ & $b$ & \color{blue}{$a$} & \color{blue}{$1$} \\
\color{blue}{$0$} & $d$ & $a$ & $b$ & \color{blue}{$a$} & \color{blue}{$1$} \\
\color{blue}{$2$} & $r$ & $a$ & $\fish$ & \color{blue}{$b$} & \color{blue}{$1$} \\
\color{blue}{$2$} & $r$ & $a$ & $c$ & \color{blue}{$b$} & \color{blue}{$1$} \\
/ & $r$ & $a$ & $d$ & \color{blue}{$b$} & \color{blue}{$1$} \\
\hline
\end{tabular} %
\begin{tabular}{|@{}c@{}|}
\hline
$\mbox{\texttt{in}}^R$ \\
\hline
\color{blue}{$1$}\\
\color{blue}{$1$}\\
\color{blue}{$1$}\\
\color{blue}{$1$}\\
\color{blue}{$1$}\\
\color{blue}{$0$}\\
\color{blue}{$0$}\\
\color{blue}{$1$}\\
\color{blue}{$1$}\\
\color{blue}{$1$}\\
\color{blue}{$1$}\\
\color{blue}{$1$}\\
\color{blue}{$1$}\\
\hline
\end{tabular}
\end{center}
}

\frame{
\frametitle{Complexity}
\begin{itemize}
\item The BVDBG takes $\mathcal{O}\left(n_{edges} \log\left|\Sigma\right| + n_{nodes} \log(k)\right)$ bits of space.
\item Each query can be done in $\mathcal{O}\left(\log(k) + \left|\Sigma\right|\right)$.
\end{itemize}

We have implemented this data structure in C++ for further evaluation.
}


\section{Experimentation}

\subsection{Space cost}
\frame{
\frametitle{Space cost}
\resizebox{\textwidth}{!}{
\begin{tikzpicture}[scale = 0.6]
\begin{axis}[title=\textit{Staphilicocus Aureus}, xlabel={$k$}, ylabel={Size in Mbytes}, grid=major, legend entries={BVDBG, FM-index}, legend pos = north west]
\addplot+[smooth, mark=+] coordinates {
(5, 10.048847)
(10, 45.876462)
(15, 199.373209)
(20, 244.081283)
(25, 274.412049)
(30, 302.110319)
(35, 327.913459)
(40, 351.823210)
(45, 374.313401)
(50, 395.516483)
(55, 415.756748)
(60, 435.065086)
(65, 453.391164)
(70, 470.467729)
(75, 486.260588)
(80, 500.113736)
(85, 512.146462)
(90, 522.252589)
(95, 529.910605)
(100, 534.208469)
(105, 535.489701)
(110, 535.589012)
(115, 535.563283)
(120, 535.609893)
};
\addplot[red, domain = 0:120, samples = 200, thick] {120.649436};
\end{axis}
\end{tikzpicture}
\begin{tikzpicture}[scale = 0.6]
\begin{axis}[title=\textit{Rhodobacter sphaeroides}, xlabel={$k$}, ylabel={Size in Mbytes}, grid=major, legend entries={BVDBG, FM-index}, legend pos = north west]
\addplot+[smooth, mark=+] coordinates {
(5, 13.955181)
(10, 66.123696)
(15, 269.145119)
(20, 393.361365)
(25, 460.044963)
(30, 516.336665)
(35, 568.688272)
(40, 618.141333)
(45, 665.269892)
(50, 709.781804)
(55, 751.599578)
(60, 790.878965)
(65, 827.401394)
(70, 860.746925)
(75, 891.029971)
(80, 917.579622)
(85, 940.139129)
(90, 958.263555)
(95, 971.061220)
(100, 978.109515)
(105, 980.874492)
(110, 982.515193)
(115, 982.774846)
(120, 982.988265)
};
\addplot[red, domain = 0:120, samples = 200, thick] {232.273990};
\end{axis}
\end{tikzpicture}
}
\begin{description}
\item[\textit{Staphilicocus Aureus}] 1,294,104 reads of length 101bp.
\item[\textit{Rhodobacter sphaeroides}] 2,050,868 reads of length 101bp.
\end{description}
}

\subsection{Time cost}
\frame{
\frametitle{Time cost}
\resizebox{\textwidth}{!}{
\begin{tabular}{|l|c|c|c|c|c|c|}
\hline
& & \multicolumn{2}{c|}{\textit{Staphylococcus aureus}} & \multicolumn{2}{c|}{\textit{Rhodobacter sphaeroides}} \\
\hline
query & $k$ & mean (\micro\second) & deviation (\micro\second) & mean (\micro\second) & deviation (\micro\second) \\
\hline
\multirow{4}{*}{\texttt{check}} & 10 & 0.8 & 0.7 & 0.9 & 0.7 \\
 & 30 & 0.7 & 0.8 & 0.7 & 0.7 \\
 & 50 & 0.7 & 0.8 & 0.7 & 0.7 \\
 & 100 & 0.7 & 0.7 & 0.6 & 0.7\\
 \hline
 \multirow{4}{*}{\texttt{add}} & 10 & 2.7 & 0.9 & 2.9 & 0.9 \\
 & 30 & 1.9 & 1.3 & 1.9 & 1.1 \\
 & 50 & 1.7 & 1.3 & 1.7 & 1.1 \\
 & 100 & 1.6 & 1.0 & 1.6 & 1.0 \\
 \hline
 \multirow{4}{*}{\texttt{del}} & 10 & 1.9 & 1.0 & 2.0 & 0.9 \\
 & 30 & 1.6 & 1.2 & 1.5 & 1.0 \\
 & 50 & 1.5 & 1.3 & 1.4 & 1.1 \\
 & 100 & 1.4 & 1.1 & 1.3 & 1.0\\
\hline
\end{tabular}}
}

\section{Further work}

\frame{
\frametitle{Further work}
\begin{itemize}
\item<1-> Test our BVDBG on larger datasets (Human 14).
\item<2-> Improve the construction of the BVDBG.
\item<3-> Try to implement and adapt variable-order assembly.
\end{itemize}
}

\beginbackup

\frame{
\frametitle{Omnitigs Assembly}
\begin{columns}
  \begin{column}{0.5\textwidth}
    Omnitigs are paths that appears in all circular, edge-covering paths of a strongly connected graph.
    
    \onslide<2->{
    Here we have the omnitigs:
    \begin{itemize}
    \item $aaaabbb$
    \item $aaabbbaaa$
    \item $aaabbbcbaaa$
    \item $baaaa$
    \end{itemize}}
  \end{column}
  \begin{column}{0.5\textwidth}
    \begin{center}
    \resizebox{\textwidth}{!}{
        \begin{tikzpicture}[scale = 2, ->]
        \node (aaa) at (-0.7,0.7) {$aaa$};
        \node (aab) at (-1, 0) {$aab$};
        \node (abb) at (-0.7, -0.7) {$abb$};
        \node (bbb) at (0, -1) {$bbb$};
        \node (bba) at (0, 0) {$bba$};
        \node (baa) at (0, 1) {$baa$};
        \node (bbc) at (0.7, -0.7) {$bbc$};
        \node (bcb) at (1, 0) {$bcb$};
        \node (cba) at (0.7, 0.7) {$cba$};
        
        \draw (baa) to [out = 180, in = 45] (aaa);
        \draw (aaa) to [out = 225, in = 90] (aab);
        \draw (aab) to [out = 270, in = 135] (abb);
        \draw (abb) to [out = 315, in = 180] (bbb);
        \draw (bbb) to [out = 0, in = 225] (bbc);
        \draw (bbc) to [out = 45, in = 270] (bcb);
        \draw (bcb) to [out = 90, in = 315] (cba);
        \draw (cba) to [out = 135, in = 0] (baa);
        
        \draw (aaa) to [out = 90, in = 180, looseness = 6] (aaa);
        \draw (bbb) to (bba);
        \draw (bba) to (baa);
        \end{tikzpicture}}
    \end{center}
  \end{column}
\end{columns}
}

\frame{
\frametitle{Omnitigs results}
\begin{center}
\begin{tabular}{cc}
\begin{tikzpicture}[scale = 0.45]
\begin{semilogyaxis}[title = Time, xlabel={$k$}, ylabel={time (\second)}, grid=major, legend pos = north west]
\addplot+[mark=+] coordinates {
(1, 0.000921)
(2, 0.004953)
(3, 0.020355)
(4, 0.079752)
(5, 0.305506)
(6, 1.1148)
(7, 98.1406)
(8, 5835.41)
(9, 7400.23)
(10, 2873.38)
(11, 843.438)
(12, 217.736)
(13, 45.2111)
(14, 8.56832)
};
\end{semilogyaxis}
\end{tikzpicture}&
\begin{tikzpicture}[scale = 0.45]
\begin{semilogyaxis}[title = Number of Omnitigs, xlabel={$k$}, ylabel={number}, grid=major, legend pos = north west]
\addplot+[mark=+] coordinates {
(1, 16)
(2, 64)
(3, 256)
(4, 1024)
(5, 4096)
(6, 16350)
(7, 50531)
(8, 53964)
(9, 22908)
(10, 6809)
(11, 1754)
(12, 444)
(13, 95)
(14, 12)
};
\end{semilogyaxis}
\end{tikzpicture}\\
\begin{tikzpicture}[scale = 0.45]
\begin{semilogyaxis}[title = Length of Omnitigs, xlabel={$k$}, ylabel={length}, grid=major, legend pos = north west, legend entries={min, mean, max}]
\addplot+[mark=+] coordinates {
(1, 2)
(2, 3)
(3, 4)
(4, 5)
(5, 6)
(6, 7)
(7, 8)
(8, 9)
(9, 10)
(10, 11)
(11, 12)
(12, 13)
(13, 18)
(14, 1393)
};
\addplot+[smooth, mark=+] coordinates {
(1, 2)
(2, 3)
(3, 4)
(4, 5)
(5, 6)
(6, 7)
(7, 8.02765)
(8, 9.71755)
(9, 13.5373)
(10, 24.9605)
(11, 68.2839)
(12, 237.493)
(13, 1086.58)
(14, 8506)
};
\addplot+[smooth, mark=+] coordinates {
(1, 2)
(2, 3)
(3, 4)
(4, 5)
(5, 6)
(6, 7)
(7, 10)
(8, 20)
(9, 43)
(10, 134)
(11, 448)
(12, 1778)
(13, 5484)
(14, 20039)
};
\end{semilogyaxis}
\end{tikzpicture}&
\begin{tikzpicture}[scale = 0.45]
\begin{semilogyaxis}[title = Cumulative length, xlabel={$k$}, ylabel={length}, grid=major, legend pos = south east, legend entries={Omnitigs, Original text}]
\addplot+[mark=+] coordinates {
(1, 32)
(2, 192)
(3, 1024)
(4, 5120)
(5, 24576)
(6, 114450)
(7, 405645)
(8, 524398)
(9, 310113)
(10, 169956)
(11, 119770)
(12, 105447)
(13, 103225)
(14, 102072)
};
\addplot[domain = 0:14, samples = 200, red] {100000};
\end{semilogyaxis}
\end{tikzpicture}
\end{tabular}
\end{center}
}

\frame{
\frametitle{Rank on Bitvectors}
\begin{enumerate}
\item<1-> Divide the vector into blocks of length $l$.
\item<2-> Precompute $\mbox{\texttt{rank}}_1$ for the beginning of each blocks ($\mathcal{O}\left(\frac{n}{l} \log(n)\right)$ bits)
\item<3-> Divide again each of these blocks in sub-blocks of size $m$ and precompute $\mbox{\texttt{rank}}_1$ only for the block ($\mathcal{O}\left(\frac{n}{m} \log(l)\right)$ bits)
\item<4-> Compute a final table that contains the results for all the possible sub-blocks ($\mathcal{O}\left(m2^m\log(m)\right))$ bits)
\end{enumerate}

\onslide<5->{
If we choose for example $l = \log_2(n)$ and $m = \log_2(l)$, we achieve a total space complexity of $\mathcal{O}(n)$.
}
}

\frame{
\frametitle{Rank on Wavelet Trees}
\begin{columns}
  \begin{column}{0.5\textwidth}
    Query $\mbox{\texttt{rank}}(9, b)$, the code for $b$ is $010$:
    \begin{enumerate}
    \item<2-> $x_0 = \mbox{\texttt{b.rank}}_0(9) = 7$
    \item<3-> $x_{01} = \mbox{\texttt{b}}_0\mbox{\texttt{.rank}}_1(x_0) = 3$
    \item<4-> $\mbox{\texttt{rank}}(9, b) = \mbox{\texttt{b}}_{01}\mbox{\texttt{.rank}}_0(x_{01}) = 2$
    \end{enumerate}
    
  \end{column}
  \begin{column}{0.5\textwidth}
    \begin{center}
    \resizebox{\textwidth}{!}{
    \begin{tikzpicture}[scale = 0.0065\textwidth]
    \tikzstyle{node}=[align = center]
    
    \node[node] (s) at (4, 6) {
    \begin{tabular}{l@{$ = $}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}}
    $\Sigma$ &\multicolumn{11}{@{}l@{}}{$\{a, b, c, d, r\}$}\\
    $s$ & $a$ & $b$ & $r$ & $a$ & $c$ & $a$ & $d$ & $a$ & $b$ & $r$ & $a$\\
    $\mbox{\texttt{b}}$ & \color{blue}{$0$} & \color{blue}{$0$} & \color{blue}{$1$} & \color{blue}{$0$} & \color{blue}{$0$} & \color{blue}{$0$} & \color{blue}{$1$} & \color{blue}{$0$} & \color{blue}{$0$} & \color{blue}{$1$} & \color{blue}{$0$}
    \end{tabular}
    };
    \node[node] (s0) at (2.5, 4) {
    \begin{tabular}{l@{$ = $}c@{}c@{}c@{}c@{}c@{}c@{}c@{}c@{}}
    $\Sigma_0$ & \multicolumn{8}{@{}l}{$\{a, b, c\}$}\\
    $s_0$ & $a$ & $b$ & $a$ & $c$ & $a$ & $a$ & $b$ & $a$\\
    $\mbox{\texttt{b}}_0$ & \color{blue}{$0$} & \color{blue}{$1$} & \color{blue}{$0$} & \color{blue}{$1$} & \color{blue}{$0$} & \color{blue}{$0$} & \color{blue}{$1$} & \color{blue}{$0$}
    \end{tabular}
    };
    \node[node] (s1) at (5.5, 4) {
    \begin{tabular}{l@{$ = $}c@{}c@{}c@{}c}
    $\Sigma_1$ & \multicolumn{4}{@{}l@{}}{$\{d, r\}$}\\
    $s_1$ & $r$ & $d$ & $r$&\\
    $\mbox{\texttt{b}}_1$ & \color{blue}{$0$} & \color{blue}{$1$} & \color{blue}{$0$}&
    \end{tabular}
    };
    \node[node] (s00) at (1.75, 2.5) {\color{blue}{$a$}};
    \node[node] (s01) at (3.25, 2) {
    \begin{tabular}{l@{$ = $}c@{}c@{}c@{}c}
    $\Sigma_{01}$ & \multicolumn{4}{@{}l@{}}{$\{b, c\}$}\\
    $s_{01}$ & $b$ & $c$ & $b$&\\
    $\mbox{\texttt{b}}_{01}$ & \color{blue}{$0$} & \color{blue}{$1$} & \color{blue}{$0$}&
    \end{tabular}
    };
    \node[node] (s10) at (4.75, 2.5) {\color{blue}{$d$}};
    \node[node] (s11) at (6.25, 2.5) {\color{blue}{$r$}};
    \node[node] (s010) at (2.75, 0.5) {\color{blue}{$b$}};
    \node[node] (s011) at (3.75, 0.5) {\color{blue}{$c$}};
    
    \draw (s) -- (s0);
    \draw (s) -- (s1);
    \draw (s0) -- (s00);
    \draw (s0) -- (s01);
    \draw (s01) -- (s010);
    \draw (s01) -- (s011);
    \draw (s1) -- (s10);
    \draw (s1) -- (s11);
    \end{tikzpicture}}
    \end{center}
  \end{column}
\end{columns}
}

\frame{
\frametitle{Linear time suffix array construction}

\begin{tabular}{cccccccccccccc}
$0$ & $1$ & $2$ & $3$ & $4$ & $5$ & $6$ & $7$ & $8$ & $9$ & $10$ & $11$ & $12$ & $13$\\
\hline
$a$ & $b$ & $r$ & $a$ & $c$ & $a$ & $b$ & $r$ & $a$ & $d$ & $a$ & $b$ & $r$ & $a$
\end{tabular}

\begin{enumerate}
\item<2-> Extract 3-length words that starts at positions not divisible by 3, give them unique order-preserving numbers.

$(bra)(cab)(rad)(abr)(a\fish\fish)(rac)(abr)(ada)(bra)(\fish\fish\fish)$
\item<3-> Recursively sort the suffixes of this new string of size $\frac{2}{3} n$.

$14, 13, 10, 5, 8, 11, 1, 4, 2, 7$
\item<4-> Sort the suffixes starting at position divisible by 3 using the results of last step.

$0, 3, 6, 9, 12$
\item<5-> Merge the two arrays.

$14, 13, 10, 0, 5, 3, 8, 11, 1, 6, 4, 9, 12, 2, 7$

\end{enumerate}
}

\backupend


\end{document}