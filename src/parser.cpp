
#include "parser.hpp"
#include <iostream>
#include <fstream>




void process_line(std::string &res, const std::string &line, int k) {
  res.append(line);
  res[res.size() - 1] = '$';
  for (int i = 1; i < k; ++i) {
    res.push_back('$');
  }
}

void parse(std::string filename, std::string &res, int k) {
  std::string line;
  
  std::ifstream f_in;
  f_in.open(filename, std::ios::in);
  while (!f_in.eof()) {
    std::getline(f_in, line);
    if (line[0] == 'A' || line[0] == 'T' || line[0] == 'C' || line[0] == 'G' || line[0] == 'N') {
      process_line(res, line, k);
    }
  }
  f_in.close();
}
