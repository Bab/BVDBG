#include "trie.hpp"
#include <cassert>

trie::trie() {}

void trie::add(const std::string s, int start) {
  int i = 0;
  while (start + i < s.size() && i < prefix.size() && s[start + i] == prefix[i]) {
    ++i;
  }
  if (start + i == s.size()) {
    return;
  }
  else if (i == prefix.size()) {
    if (sons.empty()) {
      prefix.resize(s.size() - start);
      for (int k = i; k < s.size() - start; ++k) {
        prefix[k] = s[start + k];
      }
      return;
    } else {
      sons[s[start+i]].add(s, start+i+1);
      return;
    }
  }
  else {
    trie v2;
    v2.prefix.resize(prefix.size() - i - 1);
    for (int k = 0; k < prefix.size() - i - 1; ++k) {
      v2.prefix[k] = prefix[i+1+k];
    }
    v2.sons = sons;
    sons.clear();
    sons[prefix[i]] = v2;
    prefix.resize(i);
    sons[s[start+i]].add(s, start+i+1);
    return;
  }
}

std::vector<std::string> trie::recover() const {
  std::vector<std::string> res;
  if (sons.empty()) {
    res.push_back(prefix);
  }
  else {
    for(auto it = sons.cbegin(); it != sons.cend(); ++it) {
      std::vector<std::string> res_son = (*it).second.recover();
      int i_res = res.size();
      res.resize(res.size() + res_son.size());
      for (int i = 0; i < res_son.size(); ++i) {
        res[i_res] = prefix;
        res[i_res].push_back((*it).first);
        res[i_res].append(res_son[i]);
        ++i_res;
      }
    }
  }
  return res;
}
  
