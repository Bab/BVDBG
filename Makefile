CC = g++
CFLAGS = -std=c++11 -Wall -DNDEBUG -O3
IFLAGS = -Iinclude -Isdsl_build/include
LFLAGS = -Lsdsl_build/lib -lsdsl -ldivsufsort -ldivsufsort64
         

EXEC_CPP     = $(wildcard test/*.cpp)
OTHER_CPP    = $(wildcard src/*.cpp)

HPP_FILES    = $(patsubst src/*.cpp include/*.hpp $(OTHER_CPP))

EXEC_O       = $(patsubst test/%.cpp, obj/%.o, $(EXEC_CPP))
OTHER_O      = $(patsubst src/%.cpp, obj/%.o, $(OTHER_CPP))

EXEC         = $(patsubst test/%.cpp, %, $(EXEC_CPP))

DEP_FILES    = $(patsubst %.o, %.d, $(EXEC_O) $(OTHER_O))

SDSL_LIB     = $(wildcard sdsl_build/lib/*.a)
SDSL_INCLUDE = sdsl_build/include/sdsl/bit_vectors.hpp \
               sdsl_build/include/sdsl/wavelet_trees.hpp \
               sdsl_build/include/sdsl/construct.hpp \
               sdsl_build/include/sdsl/suffix_arrays.hpp \
               sdsl_build/include/sdsl/lcp.hpp

.PHONY: all sdsl clean clean_sdsl

all: $(EXEC)

$(EXEC): %: obj/%.o $(OTHER_O) $(SDSL_LIB)
	$(CC) $(CFLAGS) $(LFLAGS) $^ -o $@

$(OTHER_O): obj/%.o: src/%.cpp $(SDSL_INCLUDE) | obj
	$(CC) -MP -MMD -MF obj/$*.d $(CFLAGS) $(IFLAGS) -c $< -o $@

$(EXEC_O): obj/%.o: test/%.cpp $(SDSL_INCLUDE) | obj
	$(CC) -MP -MMD -MF obj/$*.d $(CFLAGS) $(IFLAGS) -c $< -o $@

obj:
	mkdir -p obj

sdsl: $(SDSL_LIB) $(SDSL_INCLUDE)

$(SDSL_LIB) $(SDSL_INCLUDE): | sdsl_build
	./sdsl-lite/install.sh ./sdsl_build

sdsl_build:
	mkdir -p sdsl_build

clean:
	rm -rf obj & rm -f $(EXEC)

clean_sdsl:
	./sdsl-lite/uninstall.sh & rm -r sdsl_build

-include $(DEP_FILES)
