This project implement a bidirectional, variable order de Bruijn Graph, and use
it to test omnitigs assembly.

After cloning the repository, type

```
git submodule init
git submodule update
```

to import the submodule sdsl-lite

then type

```
make
```

This should create a bunch of executables that can be used for testing.

The BVDBG implementation is in `bvdbg.hpp`

The files `my_wt_int.hpp`, `my_wt_pc.hpp` and `my_wt.hpp` contains versions of
sdsl's wavelet trees with an additional query (find_closest) that I didn't
manage to add cleanly.

The omnitigs implementation is in `omnitigs.hpp`.
