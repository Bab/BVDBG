#include "trie.hpp"
#include <iostream>

using namespace std;
int main() {
  trie t;
  t.add("bagf");
  t.add("bag");
  t.add("abcde");
  t.add("abc");
  t.add("abde");
  vector<string> res = t.recover();
  for (int i = 0; i < res.size(); ++i) {
    cout << res[i] << endl;
  }
}
  
  
