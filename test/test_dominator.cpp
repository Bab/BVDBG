#include "dominator.hpp"
#include "bvdbg_wrapper.hpp"
#include <iostream>
#include "bvdbg_printer.hpp"

using namespace std;

int main() {
  bvdbg<> bg(5, "aaabbbaaacbbbaaa");
  bvdbg_printer(cout, bg);
  int k = 3;
  bvdbg_wrapper<> g(bg, k);
  dfs<> df(g);
  dominator<> dom(g, df);
  
  uint64_t n = g.max_index();
  for (uint64_t i = 0; i < n; ++i) {
    bvdbg_wrapper<>::node_type v = g.orig_node(i);
    cout << i << ", " << bg.get_pattern(v) << endl;
  }
  
  cout << dom.dominate(3, 8) << endl;
  cout << dom.dominate(7, 8) << endl;
  
}
