#include "node.hpp"
#include "bvdbg.hpp"
#include "bvdbg_wrapper.hpp"
#include "dfs.hpp"

#include <vector>
#include <cstdint>
#include <iostream>

using namespace std;

int main() {
  bvdbg<> bg(5,"abracabradabra");
  bvdbg_wrapper<> g(bg, 3);
  dfs<bvdbg_wrapper<>, 0> df(g);
  
  cout << "r: " << df.pre(g.get_r()) << endl;
  
  uint64_t n = df.n_nodes();
  for (uint64_t i = 0; i < n; ++i) {
    uint64_t v = df.i_pre(i);
    cout << i << ", " << df.post(v) << ": ";
    cout << g.get_pattern(df.i_pre(i)) << "; ";
    std::vector<uint64_t> next(g.max_order());
    uint64_t ord = g.next_nodes<0>(next, v);
    for (uint64_t j = 0; j < ord; ++j) {
      cout << df.pre(next[j]) << ", ";
    }
    cout << "parent: " << df.pre(df.parent(v));
    cout << endl;
  }
  return 0;
}
  
