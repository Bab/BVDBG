#include "parser.hpp"
#include "bvdbg.hpp"
#include "bvdbg_printer.hpp"
#include <iostream>

using namespace std;

int main(int argc, char *argv[]) {
  string filename = argv[1];
  int k = atoi(argv[2]);
  string res;
  parse(filename, res, k);
  bvdbg<> bg(k, res);
  bvdbg_printer(cout, bg);
  cout << res << endl;
  return 0;
}

