#include "dominator.hpp"
#include "dfs.hpp"
#include "bvdbg_wrapper.hpp"
#include "strong_bridges.hpp"

#include <iostream>
#include <tuple>
#include <set>

using namespace std;

int main() {
  bvdbg<> bg(5, "ababacbabcaba");
  bvdbg_wrapper<> g(bg, 3);
  set<pair<uint64_t, uint64_t> > s_bridges = strong_bridges(g);
  for (auto it = s_bridges.cbegin(); it != s_bridges.cend(); ++it) {
    cout << bg.get_pattern(g.orig_node(get<0>(*it))) << ", " << bg.get_pattern(g.orig_node(get<1>(*it))) << endl;
  }
  return 0;
}
