#include "bvdbg.hpp"
#include "bvdbg_printer.hpp"
#include <string>
#include <iostream>

using namespace std;

int main() {
  string text("aaabaacaaabaa");
  int k = 3;
  bvdbg<> bg(k, text);
  auto v = bg.empty();
  cout << bg.check<0>(v, 'a') << endl;
  auto w = bg.add<1>(v, 'b');
  cout << bg.check<0>(w, 'a') << endl;
  
  return 0;
}
