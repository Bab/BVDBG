#include "my_wt.hpp"
#include <sdsl/wt_hutu.hpp>
#include <sdsl/construct.hpp>
#include <tuple>
#include <iostream>
using namespace std;

template<bool after = false, bool greater = false>
bool test_correctness(sdsl::int_vector<8> vec, uint64_t i, uint64_t c, pair<bool, uint64_t> res) {
  bool found = std::get<0>(res);
  uint64_t j = std::get<1>(res);
  uint64_t k;
  uint64_t begin, end;
  
  if (found) {
    if (vec[j] == c) {
      return false;
    }
    if (after != i <= j || greater != (vec[j] > c)) {
      return false;
    }
    if (after) {
      begin = i+1;
      end = j;
    } else {
      begin = j+1;
      end = i;
    }
  }
  else {
    if (after) {
      begin = i;
      end = vec.size();
    } else {
      begin = 0;
      end = i;
    }
  }
  for (k=begin; k < end; ++k) {
    if ((!greater && vec[k] < c) || (greater && vec[k] > c)) {
      return false;
    }
  }
  return true;
}

int main(int argc, char *argv[]) {
  
  int n = 10;
  if (argc > 1) {
    n = atoi(argv[1]);
  }
  unsigned int seed = time(NULL);
  if (argc > 2) {
    seed = atoi(argv[2]);
  }
  cout << "n: " << n << std::endl;
  cout << "seed: " << seed << std::endl;
  srand(seed);
  sdsl::int_vector<8> vec(n, 0);
  int k;
  for (k = 0; k < n; ++k) {
    uint64_t content = rand() % 256;
    vec[k] = content;
    if (n < 30) {
      cout << content << ", ";
    }
  }
  if (n < 30) {
    cout << endl;
  }
  my_wt_hutu<> wt;
  sdsl::construct_im(wt, vec, 0);
  int test_failed = 0;
  for (k=0; k<100*n; ++k) {
    uint64_t i = rand() % (n+1);
    uint64_t c = rand() % 256;
    pair<bool, uint64_t> p00 = wt.find_closest<0, 0>(i, c);
    pair<bool, uint64_t> p01 = wt.find_closest<0, 1>(i, c);
    pair<bool, uint64_t> p10 = wt.find_closest<1, 0>(i, c);
    pair<bool, uint64_t> p11 = wt.find_closest<1, 1>(i, c);
    if (!(test_correctness<0, 0>(vec, i, c, p00))) {
      cout << "i: " << i << "; c: " << (int) c;
      cout << "; after, greater: " << 0 << ", "  << 0;
      cout << "; p: " << get<0>(p00) << ", " << get<1>(p00) << endl;
      ++test_failed;
    }
    if (!(test_correctness<0, 1>(vec, i, c, p01))) {
      cout << "i: " << i << "; c: " << (int) c;
      cout << "; after, greater: " << 0 << ", "  << 1;
      cout << "; p: " << get<0>(p01) << ", " << get<1>(p01) << endl;
      ++test_failed;
    }
    if (!(test_correctness<1, 0>(vec, i, c, p10))) {
      cout << "i: " << i << "; c: " << (int) c;
      cout << "; after, greater: " << 1 << ", "  << 0;
      cout << "; p: " << get<0>(p10) << ", " << get<1>(p10) << endl;
      ++test_failed;
    }
    if (!(test_correctness<1, 1>(vec, i, c, p11))) {
      cout << "i: " << i << "; c: " << (int) c;
      cout << "; after, greater: " << 1 << ", "  << 1;
      cout << "; p: " << get<0>(p11) << ", " << get<1>(p11) << endl;
      ++test_failed;
    }
  }
  cout << "test_failed: " << test_failed <<  "/" << 4*100*n << endl;
  return 0;

}
