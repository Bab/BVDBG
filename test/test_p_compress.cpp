#include "path_compression.hpp"
#include <iostream>

class B_test {
  public:
    unsigned int v;
    B_test() {}
    B_test(unsigned int a): v(a) {}
    friend B_test operator* (B_test a, B_test b) {
      return a;
    }
};
  

int main() {
  std::vector<B_test> default_label (10, 0);
  for (unsigned int i = 0; i < 10; ++i) {
    default_label[i] = i;
  }
  path_compression<B_test> p_c(default_label);
  p_c.link(0, 1);
  std::cout << p_c.eval(1).v << std::endl;
  p_c.link(2, 3);
  std::cout << p_c.eval(3).v << std::endl;
  p_c.link(0, 2);
  std::cout << p_c.eval(3).v << std::endl;
  return 0;
}
  
