#include "bvdbg.hpp"
#include "bvdbg_printer.hpp"
#include <string>
#include <time.h>
#include <iostream>
#include <vector>

using namespace std;

int main(int argc, char *argv[]) {
  uint n = 20;
  if (argc > 1) {
    n = atoi(argv[1]);
  }
  uint k = 4;
  if (argc > 2) {
    k = atoi(argv[2]);
  }
  unsigned int seed = time(NULL);
  if (argc > 3) {
    seed = atoi(argv[3]);
  }
  cout << "n: " << n << std::endl;
  cout << "k: " << k << std::endl;
  cout << "seed: " << seed << std::endl;
  srand(seed);
  string text;
  uint i = 0;
  for (i=0; i<n; ++i) {
    text.push_back('a' + (rand() % ('e' - 'a' + 1)));
  }
  bvdbg<> bg(k, text);
  auto v = bg.empty();
  // Forward test
  for (i = 0; i < k; ++i) {
    assert(bg.check<1>(v, text[i]));
    v = bg.add<1>(v, text[i]);
  }
  cout << "increase k done" << endl;
  for (i = k; i < n; ++i) {
    assert(bg.check<1>(v, text[i]));
    v = bg.del<0>(v);
    v = bg.add<1>(v, text[i]);
  }
  cout << "forward done" << endl;
  // Backward test with smaller k.
  for (i = k; i > k/2; --i) {
    v = bg.del<1>(v);
  }
  cout << "decrease k done" << endl;
  for (i = n-k; i>0; --i) {
    assert(bg.check<0>(v, text[i-1]));
    v = bg.del<1>(v);
    v = bg.add<0>(v, text[i-1]);
  }
  cout << "backward done" << endl;
  cout << "No error were made" << endl;
  return 0;
}
