#include "bvdbg.hpp"
#include "parser.hpp"
#include <string>
#include <iostream>
#include <time.h>
#include <math.h>

std::pair<double, double> stats(std::vector<clock_t> v) {
  clock_t sum = 0;
  clock_t square_sum = 0;
  for (int i = 0; i < v.size(); ++i) {
    sum += v[i];
    square_sum += v[i] * v[i];
  }
  double med = ((double) sum) / v.size();
  double square_med = ((double) square_sum) / v.size();
  return {med/CLOCKS_PER_SEC, sqrt(square_med - med * med)/CLOCKS_PER_SEC};
}
  
std::pair<double, double> minmax(std::vector<clock_t> v) {
  clock_t min = v[0];
  clock_t max = v[0];
  for (int i = 1; i < v.size(); ++i) {
    min = min < v[i] ? min : v[i];
    max = max > v[i] ? max : v[i];
  }
  return {((double)min)/CLOCKS_PER_SEC, ((double)max)/CLOCKS_PER_SEC};
}

int main(int argc, char *argv[]) {
  int k = atoi(argv[1]);
  std::string text;
  clock_t t0 = clock();
  for (int i = 2; i < argc; ++i) {
    std::string filename(argv[i]);
    parse(filename, text, 1);
  }
  clock_t t_parse = clock();
  //bvdbg<> bg(k, text);
  //clock_t t_bg = clock();
  
  std::cout << "parse time: " << ((float) (t_parse - t0))/CLOCKS_PER_SEC << " seconds" << std::endl;
  std::cout << "text size: " << text.size() << std::endl;
  
  for (k = 5; k <= 150; k+=5) {
    std::cout << std::endl;
    std::cout << "k = " << k << std::endl;
    t0 = clock();
    bvdbg<> bg(k, text);
    clock_t t_bg = clock();

    std::cout << "construction time: " << ((float) (t_bg - t0))/CLOCKS_PER_SEC << " seconds" << std::endl;
    std::cout << "graph size: " << bg.byte_size() << " bytes" << std::endl;
    std::cout << "including lcs: " << bg.lcs_byte_size() << " bytes" << std::endl;
  
    std::vector<clock_t> t_add;
  
    std::vector<clock_t> t_del;
  
    std::vector<clock_t> t_check;
  
    auto v = bg.empty();
  
    for (int i = 0; i < 10000000; ++i) {
      bool right = rand() % 2;
      int choice = rand() % 5;
      if (choice == 0 && v.k > 0) {
        if (right) {
          clock_t t00 = clock();
          v = bg.del<1>(v);
          clock_t t01 = clock();
          t_del.push_back(t01 - t00);
  
        } else {
          clock_t t00 = clock();
          v = bg.del<0>(v);
          clock_t t01 = clock();
          t_del.push_back(t01 - t00);
        }
      } else if (v.k < k) {
        char c;
        switch (choice) {
          case 1: c = 'A'; break;
          case 2: c = 'T'; break;
          case 3: c = 'C'; break;
          case 4: c = 'G'; break;
        }
        bool res_check;
        if (right) {
          clock_t t00 = clock();
          res_check = bg.check<1>(v, c);
          clock_t t01 = clock();
          t_check.push_back(t01 - t00);
        } else {
          clock_t t00 = clock();
          res_check = bg.check<0>(v, c);
          clock_t t01 = clock();
          t_check.push_back(t01 - t00);
        }
        if (res_check) {
          if (right) {
            clock_t t00 = clock();
            v = bg.add<1>(v, c);
            clock_t t01 = clock();
            t_add.push_back(t01 - t00);
          } else {
            clock_t t00 = clock();
            v = bg.add<0>(v, c);
            clock_t t01 = clock();
            t_add.push_back(t01 - t00);
          }
        }
      }
    }
    std::pair<double, double> stats_check = stats(t_check);
    std::pair<double, double> stats_add = stats(t_add);
    std::pair<double, double> stats_del = stats(t_del);
    std::pair<double, double> minmax_check = minmax(t_check);
    std::pair<double, double> minmax_add = minmax(t_add);
    std::pair<double, double> minmax_del = minmax(t_del);
    
    std::cout << "check time: " << std::get<0>(stats_check);
    std::cout << "; sigma: " << std::get<1>(stats_check);
    std::cout << "; min / max: " << std::get<0>(minmax_check);
    std::cout << ", " << std::get<1>(minmax_check) <<std::endl;
    
    std::cout << "add time: " << std::get<0>(stats_add);
    std::cout << "; sigma: " << std::get<1>(stats_add);
    std::cout << "; min / max: " << std::get<0>(minmax_add);
    std::cout << ", " << std::get<1>(minmax_add) <<std::endl;
    
    std::cout << "del time: " << std::get<0>(stats_del);
    std::cout << "; sigma: " << std::get<1>(stats_del);
    std::cout << "; min / max: " << std::get<0>(minmax_del);
    std::cout << ", " << std::get<1>(minmax_del) <<std::endl;
  }
}
  
  
