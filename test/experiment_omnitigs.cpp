#include "bvdbg.hpp"
#include "parser.hpp"
#include "omnitigs.hpp"
#include "bvdbg_printer.hpp"
#include <string>
#include <iostream>
#include <time.h>
#include <math.h>
#include <unistd.h>
#include <assert.h>

using namespace std;
int main(int argc, char *argv[]) {
  bool verbose = false;
  unsigned int n = 1000;
  unsigned int k = 10;
  unsigned int sigma = 4;
  unsigned int l = 50;
  unsigned int m = 250;
  unsigned int seed = time(NULL);
  
  int c;
  while ((c = getopt(argc, argv, "hvn:k:a:l:m:s:")) != -1) {
    switch (c) {
      case 'v':
        verbose = true;
        break;
      case 'n':
        n = atoi(optarg);
        break;
      case 'k':
        k = atoi(optarg);
        break;
      case 'a':
        sigma = atoi(optarg);
        assert (sigma < 27);
        break;
      case 'l':
        l = atoi(optarg);
        break;
      case 'm':
        m = atoi(optarg);
        break;
      case 's':
        seed = atoi(optarg);
        break;
      default:
        cout << "use: ./" << argv[0] << endl;
        cout << "options: " << endl;
        cout << "  -h:          help" << endl;
        cout << "  -v:          verbose" << endl;
        cout << "  -n [number]: text size" << endl;
        cout << "  -k [number]: graph order" << endl;
        cout << "  -a [number]: alphabet size (< 27)" << endl;
        cout << "  -l [number]: read length" << endl;
        cout << "  -m [number]: reads number" << endl;
        cout << "  -s [number]: set seed" << endl;
        return 0;
        break;
    }
  }
  
  srand(seed);
  string text;
  for (int i = 0; i < n; ++i) {
    text.push_back('a' + (rand() % sigma));
  }
  string samples;
  for (int i = 0; i < m; ++i) {
    int i0 = rand();
    for (int j = 0; j < l; ++j) {
      samples.push_back(text[(i0+j)%n]);
    }
    samples.push_back('$');
  }
  
  cout << "n:    " << n << endl;
  cout << "k:    " << k << endl;
  cout << "a:    " << sigma << endl;
  cout << "l:    " << l << endl;
  cout << "m:    " << m << endl;
  cout << "seed: " << seed << endl;
  
  // n_r > \frac{2n}{l_r - k} \ln(\frac{2n}{l_r - k})
  
  
  if (verbose) {
    cout << "text:" << endl;
    cout << text << endl;
  }
  
  bvdbg<> bg(k, samples);
  
  for (int ki = k; ki > 0; --ki) {
    cout << endl << "k: " << ki << endl;
    clock_t t0 = clock();
    vector<string> omn = get_omnitigs(bg, ki);
    clock_t t1 = clock();
    if (verbose) {
      cout << "omnitigs:" << endl;
      for (int i = 0; i < omn.size(); ++i) {
        std::cout << omn[i] << std::endl;
      }
    }
    unsigned int max_size = 0;
    unsigned int min_size = -1;
    unsigned int total_size = 0;
    double var = 0;
    for (int i = 0; i < omn.size(); ++i) {
      unsigned int s = omn[i].size();
      max_size = s > max_size ? s : max_size;
      min_size = s < min_size ? s : min_size;
      total_size += s;
      var += ((double)s*s)/omn.size();
    }
    
    double med = ((double) total_size)/omn.size();
    var = var - med*med;
    
    cout << "found " << omn.size() << " omnitigs" << endl;
    cout << "of total size " << total_size << endl;
    cout << "of medium size " << med << " +- " << sqrt(var) << endl;
    cout << "min / max " << min_size << "/" << max_size << endl; 
    cout << "in " << ((double) (t1-t0)) / CLOCKS_PER_SEC << " seconds" << endl;
  }
}
