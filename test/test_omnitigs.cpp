#include "bvdbg.hpp"
#include "bvdbg_wrapper.hpp"
#include "bvdbg_printer.hpp"
#include "omnitigs.hpp"
#include <iostream>
#include <time.h>
#include <string>
#include <unistd.h>
#include <assert.h>

using namespace std;
int main(int argc, char *argv[]) {
  unsigned int n = 20;
  unsigned int k_bg = 5;
  unsigned int k = 3;
  unsigned int seed = time(NULL);
  unsigned int sigma = 5;
  bool text_provided = false;
  string text;
  
  int c;
  while ((c = getopt(argc, argv, "n:k:l:s:t:a:")) != -1) {
    switch (c) {
      case 'n':
        n = atoi(optarg);
        break;
      case 'k':
        k = atoi(optarg);
        break;
      case 'l':
        k_bg = atoi(optarg);
        break;
      case 's':
        seed = atoi(optarg);
        break;
      case 't':
        text = string(optarg);
        text_provided = true;
        break;
      case 'a':
        sigma = atoi(optarg);
        assert (sigma < 26);
        break;
    }
  }
  srand(seed);
  if (!text_provided) {
    for (int i = 0; i < n; ++i) {
      text.push_back('a' + (rand() % sigma));
    }
  }
  
  if (!text_provided) {
    cout << "n:       " << n << endl;
    cout << "sigma:   " << sigma << endl;
    cout << "seed:    " << seed << endl;
  }
  cout << "k_bvdbg: " << k_bg << endl;
  cout << "k:       " << k << endl;
  cout << "text:" << endl;
  cout << text << endl;
  
  
  // Circularisation
  for (int i = 0; i < k+1; ++i) {
    text.push_back(text[i]);
  }
  
  bvdbg<> bg(k_bg, text);
  cout << "bvdbg:" << endl;
  bvdbg_printer(cout, bg);
  
  vector<string> omn = get_omnitigs(bg, k);
  cout << "omnitigs:" << endl;
  for (int i = 0; i < omn.size(); ++i) {
    std::cout << omn[i] << std::endl;
  }
  
  return 0;
}

